-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Апр 07 2019 г., 16:09
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8  */;

--
-- База данных: `adminpanel`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `admin_access` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `archive` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `user`, `admin_access`, `created_at`, `modified_at`, `modified_by`, `active`, `archive`, `deleted`) VALUES
(1, 1, 7, '0000-00-00 00:00:00', '2019-03-31 19:39:19', 1, 1, 0, 0),
(9, 3, 2, '2019-02-16 05:24:19', '2019-02-16 22:49:58', 1, 1, 0, 0),
(10, 23, 7, '2019-02-20 19:13:55', '0000-00-00 00:00:00', 1, 1, 0, 0),
(11, 2, 7, '2019-03-28 07:42:21', '2019-04-01 05:17:07', 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `ip` text NOT NULL,
  `date` datetime NOT NULL,
  `action` text NOT NULL,
  `text` text NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Дамп данных таблицы `logs`
--

INSERT INTO `logs` (`id`, `user`, `ip`, `date`, `action`, `text`, `create_time`) VALUES
(1, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DISCONNECT', '', '2019-04-02 20:26:14'),
(2, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_CONNECT', '', '2019-04-02 20:26:17'),
(3, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DISCONNECT', '', '2019-04-02 22:28:05'),
(4, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_CONNECT', '', '2019-04-02 22:28:07'),
(5, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DISCONNECT', '', '2019-04-02 22:28:15'),
(6, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_CONNECT', '', '2019-04-02 22:28:17'),
(7, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DISCONNECT', '', '2019-04-02 22:28:19'),
(8, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_CONNECT', '', '2019-04-02 22:28:22'),
(9, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DISCONNECT', '', '2019-04-02 22:28:23'),
(10, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_CONNECT', '', '2019-04-02 22:28:34'),
(11, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DISCONNECT', '', '2019-04-02 22:28:36'),
(12, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_CONNECT', '', '2019-04-02 22:28:38'),
(13, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_ACC', 'ID: 1 | Nick: Rikko | Data: {nick: Rikko | email: strukov.pavel.n@gmail.com | images: logo.png | sex: 0}', '2019-04-02 22:38:39'),
(14, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DISCONNECT', '', '2019-04-02 22:38:43'),
(15, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_CONNECT', '', '2019-04-02 22:38:45'),
(16, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DISCONNECT', '', '2019-04-05 03:49:40'),
(17, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_CONNECT', '', '2019-04-05 03:49:45'),
(18, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DELETE_NEWS_CATEGORY', 'ID category [2]', '2019-04-07 04:58:55'),
(19, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DELETE_NEWS_CATEGORY', 'ID category [2]', '2019-04-07 05:00:32'),
(20, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DELETE_NEWS_CATEGORY', 'ID category [2]', '2019-04-07 05:10:24'),
(21, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_NEWS_CATEGORY', 'ID category [2] | Name [MineCraft2]', '2019-04-07 05:35:11'),
(22, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_NEWS_CATEGORY', 'ID category [2] | Name [MineCraft232]', '2019-04-07 05:37:13'),
(23, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_NEWS_CATEGORY', 'ID category [2] | Name []', '2019-04-07 05:39:37'),
(24, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_NEWS_CATEGORY', 'ID category [2] | Name []', '2019-04-07 05:39:42'),
(25, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_NEWS_CATEGORY', 'ID category [2] | Name []', '2019-04-07 05:39:46'),
(26, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_NEWS_CATEGORY', 'ID category [2] | Name []', '2019-04-07 05:39:47'),
(27, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_NEWS_CATEGORY', 'ID category [2] | Name []', '2019-04-07 05:39:47'),
(28, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_NEWS_CATEGORY', 'ID category [1] | Name []', '2019-04-07 05:39:47'),
(29, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_EDIT_NEWS_CATEGORY', 'ID category [2] | Name [MineCraft23]', '2019-04-07 05:43:00'),
(30, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_ADD_NEWS_CATEGORY', 'ID category [8] | Name [Новая]', '2019-04-07 05:51:56'),
(31, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_ADD_NEWS_CATEGORY', 'ID category [9] | Name [ии]', '2019-04-07 05:56:31'),
(32, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DELETE_NEWS_CATEGORY', 'ID category [9]', '2019-04-07 06:03:46'),
(33, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_ADD_NEWS_CATEGORY', 'ID category [10] | Name [sdfds]', '2019-04-07 06:04:03'),
(34, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DELETE_NEWS_CATEGORY', 'ID category [10] | Name: [sdfds]', '2019-04-07 16:06:54'),
(35, 1, '127.0.0.1', '0000-00-00 00:00:00', 'LOGS_DELETE_NEWS_CATEGORY', 'ID category [8] | Name: [Новая]', '2019-04-07 16:07:05');

-- --------------------------------------------------------

--
-- Структура таблицы `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `title` text NOT NULL,
  `pre_text` text NOT NULL,
  `text` text NOT NULL,
  `category` int(11) NOT NULL,
  `views` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `delay_public_time` datetime NOT NULL,
  `active` tinyint(4) NOT NULL,
  `archive` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=51 ;

--
-- Дамп данных таблицы `news`
--

INSERT INTO `news` (`id`, `user`, `title`, `pre_text`, `text`, `category`, `views`, `create_time`, `delay_public_time`, `active`, `archive`, `deleted`) VALUES
(1, 0, 'SA-MP 0.3.7 updates', '- Over 500 new object IDs added, including stunt objects and land objects.<br>\n- Interface font size changing.<br>\n- Some new variations of the San Andreas cop skins.<br>\n...', '- Over 500 new object IDs added, including stunt objects and land objects.<br>\n- Interface font size changing.<br>\n- Some new variations of the San Andreas cop skins.<br>\n- Server control of the car doors and windows.<br>\n- The ability to add sirens for unmarked cop cars.<br>\n- A simple static actor system to more easily create actor NPCs for shops.<br>\n- Many bug fixes and new scripting features.<br>\n\nSpecial thanks to Matite and GamerX server for contributing the new the object IDs. Thanks to everyone who tested the new version while it was in testing.\n\nWe hope you enjoy the new version. Please check back for any updates.', 0, 0, '2019-04-03 03:44:22', '0000-00-00 00:00:00', 0, 0, 0),
(2, 1, 'SA-MP 0.3z updates', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.\r\n\r\nWe&#039;ve managed to include a few features and fixes too.\r\n- An optional lag compensation mode improves sync accuracy.\r\n- New server callbacks allow greater control over the game.\r\n...', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.\r\n\r\nWe&#039;ve managed to include a few features and fixes too.\r\n- An optional lag compensation mode improves sync accuracy.\r\n- New server callbacks allow greater control over the game.\r\n- Network statistics functions will make it easier to admin servers.\r\n- Fixes range check errors in the server browser.\r\n- Fixes problems with mouse control in the game menu.\r\n- Many minor security updates for both the client and server.\r\n\r\nThanks to everyone who helped test 0.3z! ', 3, 0, '2019-04-03 03:44:22', '2019-04-18 05:04:00', 0, 0, 1),
(3, 0, 'SA-MP 0.3z updates', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n...', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n- Network statistics functions will make it easier to admin servers.<br>\r\n- Fixes range check errors in the server browser.<br>\r\n- Fixes problems with mouse control in the game menu.<br>\r\n- Many minor security updates for both the client and server.<br><br>\r\nThanks to everyone who helped test 0.3z! ', 0, 0, '2019-04-03 03:44:22', '0000-00-00 00:00:00', 0, 0, 0),
(4, 0, 'SA-MP 0.3z updates', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n...', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n- Network statistics functions will make it easier to admin servers.<br>\r\n- Fixes range check errors in the server browser.<br>\r\n- Fixes problems with mouse control in the game menu.<br>\r\n- Many minor security updates for both the client and server.<br><br>\r\nThanks to everyone who helped test 0.3z! ', 0, 0, '2019-04-03 03:44:22', '0000-00-00 00:00:00', 0, 0, 0),
(5, 0, 'SA-MP 0.3z updates', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n...', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n- Network statistics functions will make it easier to admin servers.<br>\r\n- Fixes range check errors in the server browser.<br>\r\n- Fixes problems with mouse control in the game menu.<br>\r\n- Many minor security updates for both the client and server.<br><br>\r\nThanks to everyone who helped test 0.3z! ', 0, 0, '2019-04-03 03:44:22', '0000-00-00 00:00:00', 0, 0, 0),
(6, 0, 'SA-MP 0.3z updates', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n...', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n- Network statistics functions will make it easier to admin servers.<br>\r\n- Fixes range check errors in the server browser.<br>\r\n- Fixes problems with mouse control in the game menu.<br>\r\n- Many minor security updates for both the client and server.<br><br>\r\nThanks to everyone who helped test 0.3z! ', 0, 0, '2019-04-03 03:44:22', '0000-00-00 00:00:00', 0, 0, 0),
(7, 0, 'SA-MP 0.3z updates', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n...', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n- Network statistics functions will make it easier to admin servers.<br>\r\n- Fixes range check errors in the server browser.<br>\r\n- Fixes problems with mouse control in the game menu.<br>\r\n- Many minor security updates for both the client and server.<br><br>\r\nThanks to everyone who helped test 0.3z! ', 0, 0, '2019-04-03 03:44:22', '0000-00-00 00:00:00', 0, 0, 0),
(8, 0, 'SA-MP 0.3z updates', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n...', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n- Network statistics functions will make it easier to admin servers.<br>\r\n- Fixes range check errors in the server browser.<br>\r\n- Fixes problems with mouse control in the game menu.<br>\r\n- Many minor security updates for both the client and server.<br><br>\r\nThanks to everyone who helped test 0.3z! ', 0, 0, '2019-04-03 03:44:22', '0000-00-00 00:00:00', 0, 0, 0),
(9, 0, 'SA-MP 0.3z updates', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n...', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.<br>\r\nWe''ve managed to include a few features and fixes too.<br><br>\r\n\r\n- An optional lag compensation mode improves sync accuracy.<br>\r\n- New server callbacks allow greater control over the game.<br>\r\n- Network statistics functions will make it easier to admin servers.<br>\r\n- Fixes range check errors in the server browser.<br>\r\n- Fixes problems with mouse control in the game menu.<br>\r\n- Many minor security updates for both the client and server.<br><br>\r\nThanks to everyone who helped test 0.3z! ', 0, 0, '2019-04-03 03:44:22', '0000-00-00 00:00:00', 0, 0, 0),
(10, 0, 'test', 'tfdds', 'SA-MP 0.3z is mainly a security update for the SA-MP 0.3 branch.&lt;br&gt;\r\nWe&#039;ve managed to include a few&#039; f&#039;eatures and fixes too.&lt;br&gt;&lt;br&gt;\r\n\r\n- An optional lag compensation mode improves sync accuracy.&lt;br&gt;\r\n- New server callbacks allow greater control over the game.&lt;br&gt;\r\n...', 0, 0, '2019-04-05 01:30:06', '1970-01-01 03:00:00', 0, 0, 1),
(11, 0, 'выа', 'ываыв<br>ываыв<br>ываыв<br>ываыв<br>ываыв<br>ываыв<br>ываыв<br>ываыв<br>ываыв<br>ываыв<br>ываыв<br>ываыв<br>', 'выаыв', 6, 0, '2019-04-05 02:11:00', '1970-01-01 03:00:00', 0, 0, 1),
(12, 0, 'выа', 'выаыв', 'ываыв', 4, 0, '2019-04-05 02:13:24', '0000-00-00 00:00:00', 0, 0, 1),
(13, 0, 'выа', 'ываыв', 'выаыв', 1, 0, '2019-04-05 02:15:37', '1970-01-01 03:00:00', 0, 0, 1),
(14, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:17:00', '0000-00-00 00:00:00', 0, 0, 1),
(15, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:17:10', '0000-00-00 00:00:00', 0, 0, 1),
(16, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:17:28', '0000-00-00 00:00:00', 0, 0, 1),
(17, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:17:28', '0000-00-00 00:00:00', 0, 0, 1),
(18, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:17:29', '0000-00-00 00:00:00', 0, 0, 1),
(19, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:17:30', '0000-00-00 00:00:00', 0, 0, 1),
(20, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:17:30', '0000-00-00 00:00:00', 0, 0, 1),
(21, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:17:48', '0000-00-00 00:00:00', 0, 0, 1),
(22, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:19:54', '0000-00-00 00:00:00', 0, 0, 1),
(23, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:19:56', '0000-00-00 00:00:00', 0, 0, 1),
(24, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:20:28', '0000-00-00 00:00:00', 0, 0, 1),
(25, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:20:30', '0000-00-00 00:00:00', 0, 0, 1),
(26, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:23:20', '0000-00-00 00:00:00', 0, 0, 1),
(27, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:23:22', '0000-00-00 00:00:00', 0, 0, 1),
(28, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:23:47', '0000-00-00 00:00:00', 0, 0, 1),
(29, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:23:49', '0000-00-00 00:00:00', 0, 0, 1),
(30, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:24:26', '0000-00-00 00:00:00', 0, 0, 1),
(31, 0, 'выа', 'g', 'gf', 6, 0, '2019-04-05 02:24:27', '0000-00-00 00:00:00', 0, 0, 1),
(32, 0, 'jh', 'h', 'g', 4, 0, '2019-04-05 02:24:33', '0000-00-00 00:00:00', 0, 0, 1),
(33, 0, 'jh', 'h', 'g', 4, 0, '2019-04-05 02:24:35', '0000-00-00 00:00:00', 0, 0, 1),
(34, 0, 'выа', 'dsd', 'sdsd', 7, 0, '2019-04-05 02:34:18', '0000-00-00 00:00:00', 0, 0, 1),
(35, 0, 'sdfgds', 'gdfgfd', 'gdfgsfd', 3, 0, '2019-04-05 02:36:15', '0000-00-00 00:00:00', 0, 0, 1),
(36, 0, 'dsfds', 'fdsfdsf', 'dsfds', 4, 0, '2019-04-05 02:37:14', '0000-00-00 00:00:00', 0, 0, 1),
(37, 0, 'fsdfsd', 'fsdfas', 'dfsdfa', 0, 0, '2019-04-05 02:38:55', '0000-00-00 00:00:00', 0, 0, 1),
(38, 0, 'fsdfsd', 'fsdfas', 'dfsdfa', 0, 0, '2019-04-05 02:39:36', '0000-00-00 00:00:00', 0, 0, 1),
(39, 0, 'ghdfgh', 'fgdhdfg', 'hdfghdfg', 0, 0, '2019-04-05 02:39:45', '0000-00-00 00:00:00', 0, 0, 1),
(40, 0, 'ghdfgh', 'fgdhdfg', 'hdfghdfg', 0, 0, '2019-04-05 02:39:57', '0000-00-00 00:00:00', 0, 0, 1),
(41, 0, 'ghdfgh', 'fgdhdfg', 'hdfghdfg', 0, 0, '2019-04-05 02:40:01', '0000-00-00 00:00:00', 0, 0, 1),
(42, 0, 'ghdfgh', 'fgdhdfg', 'hdfghdfg', 0, 0, '2019-04-05 02:43:03', '0000-00-00 00:00:00', 0, 0, 1),
(43, 0, 'ghdfgh', 'fgdhdfg', 'hdfghdfg', 0, 0, '2019-04-05 02:43:06', '0000-00-00 00:00:00', 0, 0, 1),
(44, 0, 'hgfhfg', 'dfghfg', 'hdfg', 0, 0, '2019-04-05 02:43:08', '0000-00-00 00:00:00', 0, 0, 1),
(45, 0, 'dfghfg', 'hdfghd', 'fghdfgh', 0, 0, '2019-04-05 02:43:13', '0000-00-00 00:00:00', 0, 0, 1),
(46, 0, 'dfghfg', 'hdfghd', 'fghdfgh', 0, 0, '2019-04-05 02:44:56', '0000-00-00 00:00:00', 0, 0, 1),
(47, 0, 'dfghfg', 'hdfghd', 'fghdfgh', 0, 0, '2019-04-05 02:45:28', '0000-00-00 00:00:00', 0, 0, 1),
(48, 1, 'dsfds  ', 'dsfdsf&#039;&#039;&#039;&#039;&#039;&#039;f&#039;&#039;d&#039;fdf&#039;dfd&#039;d/fdffddfd&#039;&#039;df&#039;23423421````  ', 'dsfsdfs&#039;  ', 1, 0, '2019-04-05 03:53:45', '1970-01-01 03:00:00', 0, 0, 1),
(49, 1, 'sdfsd', 'sdafasdf', 'asdfasd', 0, 0, '2019-04-05 03:54:09', '0000-00-00 00:00:00', 0, 0, 1),
(50, 1, 'Новость на главной странице   ', 'Сайт на данный момент в разработке.\r\nЕсли у Вас есть аккаунт с правами администратора, то Вы сможете перейти в админ панель.   ', 'Сайт на данный момент в разработке.\r\nЕсли у Вас есть аккаунт с правами администратора, то Вы сможете перейти в админ панель.   ', 0, 0, '2019-04-05 19:13:13', '1970-01-01 03:00:00', 1, 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `news_category`
--

CREATE TABLE IF NOT EXISTS `news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `active` tinyint(4) NOT NULL,
  `archive` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `news_category`
--

INSERT INTO `news_category` (`id`, `name`, `active`, `archive`, `deleted`) VALUES
(1, 'SA-MP', 1, 0, 0),
(2, 'MineCraft23', 1, 0, 0),
(3, 'GTA 5', 1, 0, 0),
(4, 'L4D2', 1, 0, 0),
(5, 'CS 1.6', 1, 0, 0),
(6, 'CS GO', 1, 0, 0),
(7, 'Team fortes 2', 1, 0, 0),
(8, 'Новая', 0, 0, 1),
(9, 'ии', 0, 0, 1),
(10, 'sdfds', 0, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  `sex` int(11) NOT NULL,
  `token` text NOT NULL,
  `reg_ip` text NOT NULL,
  `use_ip` text NOT NULL,
  `images` text NOT NULL,
  `created_at` datetime NOT NULL,
  `last_login_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `archive` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `email`, `sex`, `token`, `reg_ip`, `use_ip`, `images`, `created_at`, `last_login_at`, `modified_at`, `modified_by`, `active`, `archive`, `deleted`) VALUES
(1, 'Rikko', '123', 'strukov.pavel.n@gmail.com', 0, 'tyddlaypaw6c56vxq286yi93rya0uw9o6bn', '127.0.0.1', '127.0.0.1', 'logo.png', '2019-01-15 04:28:13', '2019-02-26 02:00:51', '2019-04-02 22:38:39', 1, 1, 0, 0),
(2, 'Victor', '228', 'email1333@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', 'CA.jpg', '0000-00-00 00:00:00', '2019-02-16 00:00:00', '2019-04-01 05:17:07', 1, 0, 1, 0),
(3, 'Roman', '123', 'email2@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '1022.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-04-01 04:49:19', 1, 0, 0, 1),
(4, 'Pyzo1', '123', 'email3@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0),
(5, 'Pyzo2', '123', 'email4@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-04-01 05:27:41', 1, 1, 0, 0),
(6, 'Pyzo3', '123', 'email5@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-04-01 05:27:33', 1, 1, 0, 0),
(7, 'Pyzo4', '123', 'email6@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', 'programmer.jpg', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-02-17 20:00:28', 1, 1, 0, 0),
(8, 'Pyzo5', '123', 'email7@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(9, 'Pyzo6', '123', 'email8@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(10, 'Pyzo7', '123', 'email9@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(11, 'Pyzo8', '123', 'email10@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(12, 'Pyzo9', '123', 'email11@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(13, 'Chebyrek1', '123', 'email12@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(14, 'Chebyrek2', '123', 'email13@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-02-16 22:26:30', 1, 1, 0, 0),
(15, 'Chebyrek3', '123', 'email14@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(16, 'Chebyrek4', '123', 'email15@gmail.com', 1, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(17, 'Chebyrek5', '123', 'email16@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(18, 'Chebyrek6', '123', 'email17@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(19, 'Chebyrek7', '123', 'email18@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(20, 'Chebyrek8', '123', 'email19@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(21, 'Chebyrek9', '123', 'email20@gmail.com', 0, '', '127.0.0.1', '128.33.22.12', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2019-04-01 05:27:56', 1, 1, 0, 0),
(23, 'admin', 'admin', 'admin@gmail.com', 0, 'tyddlaypaw6c56vxq286yi93rya0uw9o6bn', '127.0.0.1', '', '', '2019-02-20 19:13:37', '0000-00-00 00:00:00', '2019-02-20 19:13:55', 1, 1, 0, 0),
(24, 'admins', 'admin', 'admins@gmail.com', 0, 'weyijpf23h4mzn2bx3fvipdfi3qhnmpmtbg', '127.0.0.1', '', '', '2019-02-20 19:14:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 1, 0, 0),
(25, 'Racket', '123', 'strukov.pavel.n@gmail.com', 0, '0wgyyxdbewds21ar7flkxr4habfihd86j2', '127.0.0.1', '', '', '2019-03-29 01:40:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0),
(26, 'dfgfdgdf', '123', 'strukov.pavel.n@gmail.co', 0, 'ljw2k3ze8kxq6sml80swuixip2xt430ltm', '127.0.0.1', '', '', '2019-03-29 01:41:34', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0),
(27, 'dfgfdgdf', '123', 'strukov.pavel.n@gma', 0, '44ao0md8ayxqm3npldee4wvjpcc46o5oab', '127.0.0.1', '', '', '2019-03-29 01:42:49', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0),
(28, 'gfd', '123', 'strukov.pafl.n@gmail.com', 1, 'f8uvyi0ggf2meqd4bi4brc0j7iwyqnmw7ru0', '127.0.0.1', '', '', '2019-03-29 01:54:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
