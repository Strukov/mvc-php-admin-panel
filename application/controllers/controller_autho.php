<?php

class Controller_Autho extends Controller
{

    function __construct()
    {
        $this->model = new Model_Autho();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->get_data(0);
        $this->view->generate('autho/autho_login_view.php', 'autho/autho_view.php', $data);
    }
    function action_login()
    {
        $data = $this->model->get_data(0);
        $this->view->generate('autho/autho_login_view.php', 'autho/autho_view.php', $data);
    }
    function action_deny()
    {
        $data = $this->model->get_data(1);
        $this->view->generate('autho/autho_deny_view.php', 'autho/autho_view.php', $data);
    }
    function action_active()
    {
        $data = $this->model->get_data(2);
        $this->view->generate('autho/autho_active_view.php', 'autho/autho_view.php', $data);
    }

}