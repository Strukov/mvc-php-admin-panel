<?php
class Controller_Setting extends Controller
{

    function __construct()
    {
        $this->model = new Model_Setting();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->get_data();
        $this->view->generate('admin/setting_view.php', 'admin/template_view.php', $data);
    }
}