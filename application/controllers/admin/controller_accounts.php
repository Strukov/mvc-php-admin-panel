<?php
class Controller_Accounts extends Controller
{

    function __construct()
    {
        $this->model = new Model_Accounts();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->get_data(0);
        $this->view->generate('admin/accounts/accounts_view.php', 'admin/template_view.php', $data);
    }
    function action_open()
    {
        $data = $this->model->get_data(1);
        $this->view->generate('admin/accounts/acc_open_view.php', 'admin/template_view.php', $data);
    }
    function action_edit()
    {
        $data = $this->model->get_data(2);
        $this->view->generate('admin/accounts/acc_edit_view.php', 'admin/template_view.php', $data);
    }
    function action_add()
    {
        $data = $this->model->get_data(3);
        $this->view->generate('admin/accounts/acc_add_view.php', 'admin/template_view.php', $data);
    }
}