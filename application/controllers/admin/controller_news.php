<?php
class Controller_News extends Controller
{

    function __construct()
    {
        $this->model = new Model_News();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->get_data(0);
        $this->view->generate('admin/news/news_view.php', 'admin/template_view.php', $data);
    }
    function action_add()
    {
        $data = $this->model->get_data(1);
        $this->view->generate('admin/news/news_add_view.php', 'admin/template_view.php', $data);
    }
    function action_open()
    {
        $data = $this->model->get_data(2);
        $this->view->generate('admin/news/news_open_view.php', 'admin/template_view.php', $data);
    }
    function action_edit()
    {
        $data = $this->model->get_data(3);
        $this->view->generate('admin/news/news_edit_view.php', 'admin/template_view.php', $data);
    }
    function action_category()
    {
        $data = $this->model->get_data(4);
        $this->view->generate('admin/news/news_category_view.php', 'admin/template_view.php', $data);
    }
    function action_addcategory()
    {
        $data = $this->model->get_data(5);
        $this->view->generate('admin/news/news_add_category_view.php', 'admin/template_view.php', $data);
    }
    function action_editcategory()
    {
        $data = $this->model->get_data(6);
        $this->view->generate('admin/news/news_edit_category_view.php', 'admin/template_view.php', $data);
    }
}