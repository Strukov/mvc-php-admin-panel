<?php
class Controller_Logs extends Controller
{

    function __construct()
    {
        $this->model = new Model_Logs();
        $this->view = new View();
    }

    function action_index()
    {
        $data = $this->model->get_data();
        $this->view->generate('admin/logs/logs_view.php', 'admin/template_view.php', $data);
    }
}