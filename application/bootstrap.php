<?php

// подключаем файлы ядра
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';

require_once 'core/language.php'; // Мульти-язык (перевод текста)
require_once 'core/DB.php'; // Запросы в Базу Данных и подключение к ней
require_once 'core/plugins/logs.php'; // система логов
require_once 'core/plugins/listing.php'; // система листинга (В начало /1/2/3/4/5/ Вперед)
require_once 'core/plugins/news.php'; // система ленты новотей

require_once 'core/route.php';
Route::start(); // запускаем маршрутизатор
