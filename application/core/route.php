<?php

class Route
{

    static function start()
    {
        // контроллер и действие по умолчанию
        $controller_name = 'main';
        $action_name = 'index';
        $admin_url = '';
        $admin_panel = 0;

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        // получаем имя контроллера
        if (!empty($routes[1]) )
        {
            if(self::CheckFiltr($routes, "exit", false)==true)return true;
            if((string)$routes[1] == 'admin'){
                if(self::CheckFiltr($routes, "lang", true)==true)return true;
                $admin_url = 'admin/';
                $admin_panel = 1;
                if ( !empty($routes[2]) ){
                    $controller_name = $routes[2];
                }

                // если ты не авторизирован, то перекидываем на авторизацию
                if(isset($_COOKIE['Author'])){
                    if(DB::GetAuthorizationUser($_COOKIE['Author'])==false){
                        header('location:http://'.$_SERVER['HTTP_HOST'].'/autho/index/admin');
                        return true;
                    }
                    else{
                        $obj=json_decode($_COOKIE['Author']);
                        if(DB::GetUserAdminLevel($obj->{"id"})<1){
                            header('location:http://'.$_SERVER['HTTP_HOST'].'/autho/deny');
                            return true;
                        }
                        else{
                            $user_status=DB::GetUserStatus($obj->{"id"});
                            if($user_status==0){
                                header('location:http://'.$_SERVER['HTTP_HOST']."/autho/active");
                                return true;
                            }
                            elseif($user_status!=1){
                                header('location:http://'.$_SERVER['HTTP_HOST']."/autho/deny");
                                return true;
                            }
                        }
                    }
                }
                else{
                    header('location:http://'.$_SERVER['HTTP_HOST'].'/autho/index/admin');
                    return true;
                }
            }
            else{
                $controller_name = $routes[1];
            }
        }

        // получаем имя экшена
        if (!empty($routes[2+$admin_panel]) )
        {
            $action_name = $routes[2+$admin_panel];
        }

        // добавляем префиксы
        $model_name = 'model_'.$controller_name;
        $controller_name = 'controller_'.$controller_name;
        $action_name = 'action_'.$action_name;

        /*
                echo $model_name."<br/>";
                echo $controller_name."<br/>";
                echo $action_name."<br/>";
        */

        // подцепляем файл с классом модели (файла модели может и не быть)
        $model_file = strtolower($model_name).'.php';
        $model_path = "application/models/".$admin_url.$model_file;
        if(file_exists($model_path))
        {
            include "application/models/".$admin_url.$model_file;
        }

        // подцепляем файл с классом контроллера
        $controller_file = strtolower($controller_name).'.php';
        $controller_path = "application/controllers/".$admin_url.$controller_file;
        $infoset=0;
        if(file_exists($controller_path))
        {
            include "application/controllers/".$admin_url.$controller_file;
        }
        else
        {
            $infoset=1;
            include "application/controllers/".$admin_url."controller_404.php";
            $controller_name = 'controller_404';
            $action_name = 'action_index';
        }
        // создаем контроллер
        $controller = new $controller_name;
        $action = $action_name;

        if(method_exists($controller, $action))
        {
            // вызываем действие контроллера
            $controller->$action();
        }
        else // если несущ. метода в данном классе, то кидаем на 404
        {
            // создаем контроллер
            if(self::GetIgnore404Controller($controller_name)==true){
                if($infoset==0) include "application/controllers/".$admin_url."controller_404.php";
                $controller_name = 'controller_404';
            }
            $action_name = 'action_index';
            $controller = new $controller_name;
            $action = $action_name;
            $controller->$action();
        }
        return true;
    }

    // Проверяет, если $text есть в $url, то, выполнить переадресацию
    static function CheckFiltr($url,$text,$clear_next_value_url=false)
    {
        for($i=1;$i<count($url);$i++){
            if($url[$i] == $text){
                if ($text == 'lang'){
                    if($url[$i+1]<0)$url[$i+1]=0;
                    if($url[$i+1]>1)$url[$i+1]=1;
                    if(!(int)$url[$i+1])$url[$i+1]=0;
                    SetCookie("Lang", $url[$i+1], time() + (60 * 60 * 24 * 30 * 12), '/');
                }
                if ($text == 'exit'){
                    Logs::AddLogs("LOGS_DISCONNECT","");
                    SetCookie("Author", "0", time() - (60 * 60 * 24 * 30 * 12), '/');
                    header('location:http://'.$_SERVER['HTTP_HOST'].'/autho/login/admin');
                    return true;
                }
                header('location:'.self::FilterUrl($url, $text, $clear_next_value_url) );
                return true;
            }
        }
        return false;
    }

    // Чистит url от введеной /str/ , если true, то удалит /str/.../
    // Вернет url без /str/ , если true, то вернет без /str/.../
    static function FilterUrl($url, $str, $clear_next_value_url=false)
    {
        $new_url = 'http://'.$_SERVER['HTTP_HOST'];
        for($i=1;$i<count($url);$i++)
        {
            if($url[$i]==$str){
                if($clear_next_value_url==true){
                    $i++;
                }
                continue;
            }
            $new_url = $new_url."/".$url[$i];
        }
        return $new_url;
    }

    // Узнаем значение url: /$url/...ВОТ_ЭТО.../ если его нет, то присваиваем 0
    static function GetNextValueUrl($url)
    {
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        $value=0;
        for($i=1;$i<count($routes);$i++) {
            if ($routes[$i] == $url) {
                if($i<count($routes)){
                    $value=$routes[$i+1];
                }
                continue;
            }
        }
        return $value;
    }

    // Проверяем, есть ли введенный url. Если есть, то вернут true
    static function FindEnteredUrl($url)
    {
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        for($i=1;$i<count($routes);$i++) {
            if ($routes[$i] == $url) {
                return true;
            }
        }
        return false;
    }

    // Создаем token ключ
    static function SetTokenKey()
    {
        $tokengen = array("1","2","3","4","5","6","7","8","9","0","q","w","e","r","t","y","u","i","o","p","a","s","d","f","g","h","j","k","l","z","x","c","v","b","n","m");
        $token="";
        for($i=0;$i<count($tokengen);$i++){
            $token.=$tokengen[rand(0, count($tokengen))];
        }
        return $token;
    }

    // Проверка на игнор 404 контроллера
    static function GetIgnore404Controller($controller_name)
    {
        $urlIgnore=array("controller_accounts",
                    "controller_logs",
                    "controller_news"
        );
        for($i=0;$i<count($urlIgnore);$i++){
            if($controller_name==$urlIgnore[$i]){
                return false;
            }
        }
        return true;
    }
}
