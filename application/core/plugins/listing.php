<?php

class Listing
{
    static function ViewListing($list=1,$list_max=1,$url="",$url2="",$add_find_url="")// $url=/admin/accounts/index/list/--- $url2=/text/
    {
//        echo "Всего юзеров: ".$list_max."<br>";
//        echo "Юзеров на экран: ".MAX_DISPLAY_LIST."<br>";
//        echo "Всего страниц: ".(ceil($list_max/MAX_DISPLAY_LIST))."<br>";
//        echo "Моя страница: ".$list."<br>";
        // модуль листинга страниц
        $max_list=ceil($list_max/MAX_DISPLAY_LIST);
        if($max_list<=1)return true;// скрыть листинг если страниц меньше 2-х
        $active = "";
        // если выбранная страница >= 4, то, отобразить кнопку
        if($list>=4){
            echo "<li class=\"page-item\"> <a class=\"page-link\" href=\"http://".$_SERVER['HTTP_HOST'].$url."1".$url2.$add_find_url."\"><span>".Language::GetLang("BUTTON_BACK")."</span></a> </li>";
        }
        // если, страниц меньше 6, то, отобразить все доступные кнопки страниц
        if($max_list<6){
            for($i=1;$i<=5;$i++){
                if($max_list>=$i){
                    if($i==$list){
                        $active = "active";
                    }
                    echo "<li class=\"page-item ".$active."\"> <a class=\"page-link\" href=\"http://".$_SERVER['HTTP_HOST'].$url.$i.$url2.$add_find_url."\">".$i."</a> </li>";
                    $active = "";
                }
            }
        }
        // если, страниц >=6, то:
        else if($max_list>=6){
            // если моя страница меньше 4, то, отобразить все доступные кнопки страниц
            if($list<4){
                for($i=1;$i<=5;$i++){
                    if($max_list>=$i){
                        if($i==$list){
                            $active = "active";
                        }
                        echo "<li class=\"page-item ".$active."\"> <a class=\"page-link\" href=\"http://".$_SERVER['HTTP_HOST'].$url.$i.$url2.$add_find_url."\">".$i."</a> </li>";
                        $active = "";
                    }
                }
            }
            // если моя страница >=4, то, отобразить все кнопки листов, но, выбранная будет центральная
            else if($list>=4){
                $statick_info_list = -2;
                for($i=1;$i<=5;$i++){
                    if($max_list>=($list+$statick_info_list)){
                        if($statick_info_list==0){
                            $active = "active";
                        }
                        echo "<li class=\"page-item ".$active."\"> <a class=\"page-link\" href=\"http://".$_SERVER['HTTP_HOST'].$url.($list+$statick_info_list).$url2.$add_find_url."\">".($list+$statick_info_list)."</a> </li>";
                        $statick_info_list += 1;
                        $active = "";
                    }
                }
            }
        }
        // если я выбрал лист и после него еще есть доступные листы, то отобразить кнопку
        if($max_list>1){
            if(($max_list-$list)>0){
                echo "<li class=\"page-item\"> <a class=\"page-link\" href=\"http://".$_SERVER['HTTP_HOST'].$url.($list+1).$url2.$add_find_url."\"><span>".Language::GetLang("BUTTON_NEXT")."</span></a> </li>";
            }
        }
        return true;
    }
}