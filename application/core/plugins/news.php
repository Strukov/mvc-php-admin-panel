<?php

class News
{
    static function GetNameNews($id)
    {
        $result=DB::ConnectBD("SELECT `title` FROM `news` WHERE `id`='".$id."'");
        $obj=mysqli_fetch_assoc($result);
        if(!strlen($obj["title"])){
            return "None";
        }
        return $obj["title"];
    }
    static function OpenNewsId($id)
    {
        $result=DB::ConnectBD("SELECT * FROM `news` WHERE `id`='".$id."'");
        if($result){
            return json_encode(mysqli_fetch_assoc($result));
        }
        return false;
    }
    static function GetNewsCategory($id)
    {
        $result=DB::ConnectBD("SELECT `category` FROM `news` WHERE `id`='".$id."'");
        if($result){
            $obj=mysqli_fetch_assoc($result);
            return $obj["category"];
        }
        return false;
    }
    static function GetNameNewsCategory($id)
    {
        $result=DB::ConnectBD("SELECT `name` FROM `news_category` WHERE `id`='".$id."'");
        $obj=mysqli_fetch_assoc($result);
        if(!strlen($obj["name"])){
            return "None";
        }
        return $obj["name"];
    }
    static function GetAllNameNewsCategory()
    {
        $result=DB::ConnectBD("SELECT * FROM `news_category` WHERE `active`='1'");
        $ss=array();
        if($result){
            $rows = mysqli_num_rows($result);
            for ($i = 1; $i <= $rows; $i++) {
                $json=array("result".$i=>(mysqli_fetch_assoc($result)));
                $ss = $ss+($json);
            }
            $ss = $ss+(array("max_load"=>$rows));
        }
        return json_encode($ss);
    }
}