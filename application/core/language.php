<?php
class Language
{
    static function GetLang($num)
    {
        if(@$_COOKIE['Lang']==1) // ru
        {
            $Lang = array(
                "0"=>"Отсутствует перевод текста! ID = ".$num,
                ////////////////////// Основное
                "ADMIN_PANEL_1"=>"Админ панель",
                "ADMIN_PANEL_2"=>"Настройки админ панели",
                "ADMIN_PANEL_3"=>"Язык интерфейса",
                ////////////////////// Меню
                "MENU_1"=>"Язык",
                "MENU_2"=>"Настройки",
                "MENU_3"=>"Выйти",
                ////////////////////// Кнопки
                "BUTTON_1"=>"Аккаунты",
                "BUTTON_2"=>"Сохранить",
                "BUTTON_3"=>"Вернуться",
                "BUTTON_4"=>"Создать",
                "BUTTON_NEXT"=>"Вперед",
                "BUTTON_FIND"=>"Найти",
                "BUTTON_BACK"=>"В начало",
                "BUTTON_TO_CANCEL"=>"Отменить",
                "BUTTON_ENTERED"=>"Ввести",
                "BUTTON_LOGS"=>"Логи действий",
                "BUTTON_NEWS"=>"Лента новостей",
                ////////////////////// Админ уровень
                "ADMIN_LVL_0"=>"Пользователь",
                "ADMIN_LVL_1"=>"Администратор 1-го уровня",
                "ADMIN_LVL_2"=>"Администратор 2-го уровня",
                "ADMIN_LVL_3"=>"Администратор 3-го уровня",
                "ADMIN_LVL_4"=>"Администратор 4-го уровня",
                "ADMIN_LVL_5"=>"Администратор 5-го уровня",
                "ADMIN_LVL_6"=>"Администратор 6-го уровня",
                "ADMIN_LVL_7"=>"Администратор 7-го уровня",
                ////////////////////// Статусы
                "STATUS_ACTIVE"=>"Активный",
                "STATUS_ARCHIVE"=>"В архиве",
                "STATUS_DELETED"=>"Удален",
                "STATUS_NO_ACTIVE"=>"Не активировынный",
                ////////////////////// Управление пользователями
                "ACC_CONTROL_1"=>"Статус аккаунта",
                "ACC_CONTROL_2"=>"Группа",
                "ACC_CONTROL_3"=>"Дата последнего редактирования",
                "ACC_CONTROL_4"=>"Редактировал",
                "ACC_CONTROL_5"=>"Редактировать аккаунт",
                "ACC_CONTROL_6"=>"Основная информация",
                "ACC_CONTROL_7"=>"Зарегистрирован",
                "ACC_CONTROL_8"=>"Последняя активность",
                "ACC_CONTROL_9"=>"Зарегистрированный IP",
                "ACC_CONTROL_10"=>"Последний IP",
                "ACC_CONTROL_11"=>"Пол",
                "ACC_CONTROL_12"=>"Женский",
                "ACC_CONTROL_13"=>"Мужской",
                "ACC_CONTROL_14"=>"Загрузить аватар",
                "ACC_CONTROL_15"=>"Редактирование пользователя",
                "ACC_CONTROL_16"=>"Ник",
                "ACC_CONTROL_ALL_ACC"=>"Список всех аккаунтов",
                "ACC_CONTROL_NEW_PASS"=>"Новый пароль",
                "ACC_CONTROL_RE_NEW_PASS"=>"Повторите новый пароль",
                "ACC_CONTROL_CREATE_ACC"=>"Создать аккаунт",
                "ACC_CONTROL_FIND_KEY"=>"Поиск по ключу",
                "ACC_CONTROL_ACC_CREATED"=>"Аккаунт создан!",
                "ACC_CONTROL_GOOD_CREATE_ACC"=>"Вы успешно создали новый аккаунт.",
                "ACC_CONTROL_NICK"=>"Ник",
                "ACC_CONTROL_GROUP"=>"Группа",
                "ACC_CONTROL_REG_IP"=>"Зарегистрированный IP",
                "ACC_CONTROL_USE_IP"=>"Последний IP",
                "ACC_CONTROL_REG_DATE"=>"Дата регистрации",
                "ACC_CONTROL_LAST_LOGIN"=>"Последняя авторизация",
                "ACC_CONTROL_STATUS"=>"Статус",
                ////////////////////// Лог действий
                "LOGS_ACTION_LOG"=>"Лог действий",
                "LOGS_ACTION"=>"Действия",
                "LOGS_CONNECT"=>"Вошел в админ панель",
                "LOGS_DISCONNECT"=>"Вышел из админ панели",
                "LOGS_LOGIN_IN"=>"Авторизовался",
                "LOGS_CREATE_ACC"=>"Создал новый аккаунт",
                "LOGS_EDIT_ACC"=>"Редактировал аккаунт",
                "LOGS_SET_ADMIN"=>"Установил лвл админа",
                "LOGS_GIVE_ADMIN"=>"Выдал права администратора",
                "LOGS_DELETE_NEWS_CATEGORY"=>"Удалил категорию постов",
                "LOGS_EDIT_NEWS_CATEGORY"=>"Изменил название категории постов",
                "LOGS_ADD_NEWS_CATEGORY"=>"Создал новую категорию постов",
            );
        }
        else// en
        {
            $Lang = array(
                "0"=>"No text translation! ID = ".$num,
                ////////////////////// Main
                "ADMIN_PANEL_1"=>"Admin panel",
                "ADMIN_PANEL_2"=>"Admin Panel Settings",
                "ADMIN_PANEL_3"=>"Interface language",
                ////////////////////// Menu
                "MENU_1"=>"Language",
                "MENU_2"=>"Setting",
                "MENU_3"=>"Exit",
                ////////////////////// Buttons
                "BUTTON_1"=>"Accounts",
                "BUTTON_2"=>"Save",
                "BUTTON_3"=>"Return",
                "BUTTON_4"=>"Create",
                "BUTTON_NEXT"=>"Next",
                "BUTTON_FIND"=>"Find",
                "BUTTON_BACK"=>"The begining",
                "BUTTON_TO_CANCEL"=>"To cancel",
                "BUTTON_ENTERED"=>"Enter",
                "BUTTON_LOGS"=>"Actions logs",
                "BUTTON_NEWS"=>"News feed",
                ////////////////////// Admin level
                "ADMIN_LVL_0"=>"User",
                "ADMIN_LVL_1"=>"Level 1 Administrator",
                "ADMIN_LVL_2"=>"Level 2 Administrator",
                "ADMIN_LVL_3"=>"Level 3 Administrator",
                "ADMIN_LVL_4"=>"Level 4 Administrator",
                "ADMIN_LVL_5"=>"Level 5 Administrator",
                "ADMIN_LVL_6"=>"Level 6 Administrator",
                "ADMIN_LVL_7"=>"Level 7 Administrator",
                ////////////////////// Статусы
                "STATUS_ACTIVE"=>"Active",
                "STATUS_ARCHIVE"=>"Archived",
                "STATUS_DELETED"=>"Deleted",
                "STATUS_NO_ACTIVE"=>"Not Activated",
                ////////////////////// User management
                "ACC_CONTROL_1"=>"Account Status",
                "ACC_CONTROL_2"=>"Group",
                "ACC_CONTROL_3"=>"Last Edited Date",
                "ACC_CONTROL_4"=>"Edited",
                "ACC_CONTROL_5"=>"Edit Account",
                "ACC_CONTROL_6"=>"Basic Information",
                "ACC_CONTROL_7"=>"Registered",
                "ACC_CONTROL_8"=>"Last activity",
                "ACC_CONTROL_9"=>"Registered IP",
                "ACC_CONTROL_10"=>"Last IP",
                "ACC_CONTROL_11"=>"Floor",
                "ACC_CONTROL_12"=>"Female",
                "ACC_CONTROL_13"=>"Male",
                "ACC_CONTROL_14"=>"Download avatar",
                "ACC_CONTROL_15"=>"User Editing",
                "ACC_CONTROL_16"=>"Nickname",
                "ACC_CONTROL_ALL_ACC"=>"List of all accounts",
                "ACC_CONTROL_NEW_PASS"=>"New Password",
                "ACC_CONTROL_RE_NEW_PASS"=>"Repeat new password",
                "ACC_CONTROL_CREATE_ACC"=>"Create an account",
                "ACC_CONTROL_FIND_KEY"=>"Search by key",
                "ACC_CONTROL_ACC_CREATED"=>"Your account has been created!",
                "ACC_CONTROL_GOOD_CREATE_ACC"=>"You have successfully created a new account.",
                "ACC_CONTROL_NICK"=>"Nick",
                "ACC_CONTROL_GROUP"=>"Group",
                "ACC_CONTROL_REG_IP"=>"Reg IP",
                "ACC_CONTROL_USE_IP"=>"Use IP",
                "ACC_CONTROL_REG_DATE"=>"Register Date",
                "ACC_CONTROL_LAST_LOGIN"=>"last login at",
                "ACC_CONTROL_STATUS"=>"Status",
                ////////////////////// Лог действий
                "LOGS_ACTION_LOG"=>"Action log",
                "LOGS_ACTION"=>"Actions",
                "LOGS_CONNECT"=>"Connecting to admin panel",
                "LOGS_DISCONNECT"=>"Disconnecting to admin panel",
                "LOGS_LOGIN_IN"=>"Авторизовался",
                "LOGS_CREATE_ACC"=>"Creates new accounts",
                "LOGS_EDIT_ACC"=>"Edited account",
                "LOGS_SET_ADMIN"=>"Set admin lvl",
                "LOGS_GIVE_ADMIN"=>"Give admin lvl",
                "LOGS_DELETE_NEWS_CATEGORY"=>"Removed post category",
                "LOGS_EDIT_NEWS_CATEGORY"=>"Changed post category name",
                "LOGS_ADD_NEWS_CATEGORY"=>"Created a new category of posts",
            );
        }
        if(is_null($Lang[$num])){
            return $Lang['0'];
        }
        return $Lang[$num];
    }
}
