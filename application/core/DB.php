<?php

class DB
{
    // Подключение к Базе данных, выполнение запроса $request, вывод результата, отключение от Базы данных.
    static function ConnectBD($request="",&$con=0)
    {
        $DB_HOST = "127.0.0.1"; // Хост
        $DB_USER = "root"; // Логин
        $DB_PASS = "";// Пароль
        $DB_NAME = "adminpanel"; // Таблица Базы Данных
        $db = mysqli_connect($DB_HOST, $DB_USER, $DB_PASS, $DB_NAME);
        if (!$db)
        {
            return "Ошибка: <br>Невозможно установить соединение с MySQL.<br>Код ошибки errno: ".mysqli_connect_errno()."<br>Текст ошибки error: ".mysqli_connect_error();
        }
        else
        {
            mysqli_set_charset($db, "utf8");// установка кодировки на utf8
            $result = mysqli_query($db, $request) or $error_code="Ошибка " . mysqli_error($db);
            $con=mysqli_insert_id($db);
            mysqli_close($db);
            if ($result)
                return $result;//json_encode(mysqli_fetch_assoc($result));
            else{
                echo "DB: ".$error_code;
                return false;
            }
        }
    }

    // Делаем запрос в Базу Данных, если успех => авторизируем и перенаправляем в админ панель, либо на гл. страницу сайта (?? - или на прошлую ссылку)
    static function AuthorizationUser($json)
    {
        $query = "SELECT `id`,`token`,`active`,`archive`,`deleted`,`login` FROM `user` WHERE `email`='".$json['email']."' AND `password`='".$json['pass']."'";
        $result = self::ConnectBD($query);
        if($result){
            $obj=mysqli_fetch_assoc($result);
            if($obj['id']>0){
                DB::ConnectBD("UPDATE `user` SET `use_ip`='".$_SERVER["REMOTE_ADDR"]."' WHERE `id`='".$obj['id']."'");
                $request=array(
                    'id' => $obj['id'],
                    'token' => $obj['token']
                );
                SetCookie("Author", json_encode($request), time() + (60 * 60 * 24 * 30 * 12), '/');
                $routes = explode('/', $_SERVER['REQUEST_URI']);
                for($i=1;$i<count($routes);$i++) {
                    if ($routes[$i] == 'admin') {
                        if($obj['active']==0&&$obj['archive']==0&&$obj['deleted']==0){
                            header('location:http://'.$_SERVER['HTTP_HOST'].'/autho/active/admin');
                            return true;
                        }
                        Logs::AddLogs("LOGS_CONNECT","",$obj['id']);
                        header('location:http://'.$_SERVER['HTTP_HOST'].'/admin');
                        return true;
                    }
                }
                if($obj['active']==0&&$obj['archive']==0&&$obj['deleted']==0){
                    header('location:http://'.$_SERVER['HTTP_HOST'].'/autho/active');
                    return true;
                }
                Logs::AddLogs($obj['login'],"Авторизовался.",$obj['id']);
                header('location:http://'.$_SERVER['HTTP_HOST']);
            }
        }
        return true;
    }

    // Проверяем авторизованность аккаунта по ID и Token ключу.
    static function GetAuthorizationUser($json)
    {
        $request = json_decode($json);
        $query = "SELECT `email` FROM `user` WHERE `id`='".$request->{'id'}."' AND `token`='".$request->{'token'}."'";
        $result = self::ConnectBD($query);
        if($result) {
            $obj=mysqli_fetch_assoc($result);
            if(strlen($obj['email'])>0){
                return true;
            }
        }
        return false;
    }

    // Узнаем статус аккаунта (no active / active / archive / deleted)
    static function GetUserStatus($id)
    {
        $status=0;
        $response=self::ConnectBD("SELECT `active`,`archive`,`deleted` FROM `user` WHERE `id`='".$id."'");
        if($response){
            $obj=mysqli_fetch_assoc($response);
            if($obj['active']==1&&$obj['archive']==0&&$obj['deleted']==0){
                $status=1;
            }
            elseif($obj['active']==0&&$obj['archive']==1&&$obj['deleted']==0){
                $status=2;
            }
            elseif($obj['active']==0&&$obj['archive']==0&&$obj['deleted']==1) {
                $status = 3;
            }
        }
        return $status;
    }

    // Узнаем Nick(login) по id запросу в Базу Данных
    static function GetUserNick($id)
    {
        $nick="None";
        if($id==0){
            return $nick;
        }
        $query = "SELECT `login` FROM `user` WHERE `id`='".$id."'";
        $result = self::ConnectBD($query);
        if($result){
            $obj=mysqli_fetch_assoc($result);
            $nick=$obj['login'];
        }
        return $nick;
    }

    // Узнаем мой Nick(login) по id запросу в Базу Данных
    static function GetMyNick()
    {
        $nick="None";
        $obj=array();
        if(isset($_COOKIE['Author'])){
            $obj=json_decode($_COOKIE['Author']);
        }
        $result = self::ConnectBD("SELECT `login` FROM `user` WHERE `id`='".$obj->{'id'}."'");
        if($result){
            $obj2=mysqli_fetch_assoc($result);
            $nick=$obj2['login'];
        }
        return $nick;
    }

    // Узнаем мой Email по id запросу в Базу Данных
    static function GetMyEmail()
    {
        $email="None";
        $obj=array();
        if(isset($_COOKIE['Author'])){
            $obj=json_decode($_COOKIE['Author']);
        }
        $result = self::ConnectBD("SELECT `email` FROM `user` WHERE `id`='".$obj->{'id'}."'");
        if($result){
            $obj2=mysqli_fetch_assoc($result);
            $email=$obj2['login'];
        }
        return $email;
    }

    // Узнаем Email пользователя по ID
    static function GetUserEmailByID($id)
    {
        $email="None";
        $result=self::ConnectBD("SELECT `email` FROM `user` WHERE `id`='".$id."'");
        if($result){
            $obj=mysqli_fetch_assoc($result);
            if(!strlen($obj["email"])){
                return $email;
            }
            else{
                $email=$obj["email"];
            }
        }
        return $email;
    }

    // Получаем информацию об аватаре, делаем проверку на наличие в папке, если есть - вывод.
    // Если нет - проверка на пол и присваиваем стандартную аватарку.
    static function GetUserAvatar($id=0,$select=true,$load_sex=0,$load_avatar="")
    {
        if($select==true){
            $query = "SELECT `images`,`sex` FROM `user` WHERE `id`='".$id."'";
            $result=self::ConnectBD($query);
            $obj=mysqli_fetch_assoc($result);
            $avatar = $obj{'images'};
            $sex=$obj{'sex'};
        }
        else{
            $avatar=$load_avatar;
            $sex=$load_sex;
        }
        if(strlen($avatar)<=0||!file_exists("./images/user/".$avatar)){
            if($sex==1)
                $avatar="no_avatar_j.png";
            else
                $avatar="no_avatar_m.png";
        }
        return $avatar;
    }

    // Узнаем уровень администратора по ID пользователя
    static function GetUserAdminLevel($id=0)
    {
        $query = "SELECT `admin_access` FROM `admin` WHERE `active`='1' AND `user`='".$id."'";
        $result=self::ConnectBD($query);
        $value=0;
        if($result){
            $obj=mysqli_fetch_assoc($result);
            if($obj["admin_access"]>0){
                $value=$obj["admin_access"];
            }
        }
        return $value;
    }
}