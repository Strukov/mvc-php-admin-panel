<div class="row">
    <div class="col-md-12 text-center">
        <h2><?php echo Language::GetLang("ADMIN_PANEL_2");?></h2>
        <br>
        <br>
    </div>
    <div class="col-md-12">
        <div class="row pi-draggable">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="card text-dark bg-muted mb-3 pi-draggable">
                    <div class="card-header text-center"><?php echo Language::GetLang("ADMIN_PANEL_3");?></div>
                    <div class="card-body">
                        <?php
                            $active[2]='';
                            if(isset($_COOKIE['Lang'])){
                                $active[$_COOKIE['Lang']]='active';
                            }
                            else{
                                $active[0]='active';
                            }
                        ?>
                        <a class="btn btn-outline-primary pi-draggable <?php echo $active[0];?>" href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."/lang/0"; ?>" draggable="true">English</a>
                        <a class="btn btn-outline-primary pi-draggable <?php echo $active[1];?>" href="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']."/lang/1"; ?>" draggable="true">Русский</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
    <div class="col-md-12"></div>
</div>