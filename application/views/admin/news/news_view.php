<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Лента новостей</h3>
        <br><br>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8" style="">

                <?php
                    $idCreatNews=Route::GetNextValueUrl("success");
                    if(Route::GetNextValueUrl("success")>0){
                        echo "  
                            <div class=\"alert alert-primary\" role=\"alert\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                                <h4 class=\"alert-heading\">Вы успешно создали новую новость!</h4>
                                <p class=\"mb-0\">Название новости: ".News::GetNameNews($idCreatNews)."</p>
                                <p class=\"mb-0\">Категория новости: ".News::GetNameNewsCategory(News::GetNewsCategory($idCreatNews))."</p>
                                <small class=\"form-text text-muted\">
                                    <a href=\"http://".$_SERVER['HTTP_HOST']."/admin/news/open/".$idCreatNews."\">Открыть созданную новость</a>
                                </small>
                            </div>
                        ";
                    }
                ?>
                <h6>Всего новостей: <?php echo $data[0]->{'max_load_rows'};?>
                <br>
                Всего категорий: <?php echo $data[1]->{'max_load_rows2'};?>
                <br>
                <?php
                    if (isset($_REQUEST['find'])) {
                        echo "Фильр по категории: ".News::GetNameNewsCategory($_REQUEST['find']);
                    }
                ?>
                </h6>
                <div class="text-right"><a class="btn btn-outline-primary" href="<?php echo "http://".$_SERVER['HTTP_HOST']."/admin/news/add"; ?>">Добавить новость</a></div>
                <br>
                <?php
                    for ($i = 1; $i <= $data[0]->{'load_rows'}; $i++)
                    {
                        $addtext="";
                        if($data[0]->{'result'.$i}->{"delay_public_time"}>0&&strtotime($data[0]->{'result'.$i}->{"delay_public_time"})>time()){
                            $addtext=" | <span class=\"text-danger\">Дата публикации: ".$data[0]->{'result'.$i}->{"delay_public_time"}."</span>";
                        }
                        echo "
                            <div class=\"card\">
                                <div class=\"card-header\">".$data[0]->{'result'.$i}->{'title'}."</div>
                                <div class=\"text-right\"><p class=\"mr-2\">Дата создания: ".$data[0]->{'result'.$i}->{'create_time'}.$addtext."</p></div>
                                <div class=\"card-body\">
                                    ".nl2br($data[0]->{'result'.$i}->{'pre_text'})."
                                </div>
                                <div class=\"text-right\"><a class=\"btn btn-outline-primary\" href=\"http://".$_SERVER['HTTP_HOST']."/admin/news/open/".$data[0]->{'result'.$i}->{'id'}."\">Подробнее</a></div>
                                <div class=\"row\">
                                    <div class=\"col-md-8\">
                                        <div class=\"ml-1\">В категории: ".News::GetNameNewsCategory($data[0]->{'result'.$i}->{'category'})."</div>
                                    </div>
                                    <div class=\"col-md-4\">
                                        <p class=\"text-right mr-2\"><i class=\"fa fa-1x fa-eye\"></i> ".$data[0]->{'result'.$i}->{'views'}."</p>
                                    </div>
                                </div>
                            </div>
                            <br>
                        ";
                    }
                ?>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <ul class="pagination" style="margin-top:4%; margin-left: 25%;">
                            <?php
                            $list = $data[0]->{'use_list'};
                            $add_find_url="";
                            if (isset($_REQUEST['find'])) {
                                $add_find_url="/?find=".$_REQUEST['find'];
                            }
                            Listing::ViewListing($list,$data[0]->{'max_load_rows'},"/admin/news/index/list/","/list_cat/".$data[1]->{'use_list2'},$add_find_url);
                            ?>
                        </ul>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="col-md-4" style="">
                <h3 class="text-center">Категории <a class="btn btn-outline-primary" href="http://<? echo $_SERVER["HTTP_HOST"];?>/admin/news/addcategory/">Создать новую</a></h3>
                <br>
                <div class="text-center">
                    <?php
                        for ($i = 1; $i <= $data[1]->{'load_rows2'}; $i++)
                        {
                            echo "
                                <a class=\"btn btn-light\" style=\"width:70%\" href=\"?find=".$data[1]->{'result_cat'.$i}->{'id'}."\">".$data[1]->{'result_cat'.$i}->{'name'}."</a>
                                <a class=\"btn btn-light\" href=\"http://".$_SERVER["HTTP_HOST"]."/admin/news/editcategory/id/".$data[1]->{'result_cat'.$i}->{'id'}."\"><i class=\"fa fa-edit fa-fw fa-1x py-1\"></i></a>
                            ";
                        }
                    ?>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <ul class="pagination" style="margin-top:4%; margin-left: 25%;">
                            <?php
                                $list = $data[1]->{'use_list2'};
                                $add_find_url="";
                                if (isset($_REQUEST['find'])) {
                                    $add_find_url="/?find=".$_REQUEST['find'];
                                }
                                Listing::ViewListing($list,$data[1]->{'max_load_rows2'},"/admin/news/index/list/".$data[0]->{'use_list'}."/list_cat/","",$add_find_url);
                            ?>
                        </ul>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>