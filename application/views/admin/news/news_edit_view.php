<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center"> Редактирование новости </div>
                <div class="card-body">
                    <form id="c_form-h" method="post">
                        <div class="form-group row"> <label for="inputtext" class="col-2 col-form-label">Наименование</label>
                            <div class="col-10">
                                <input type="text" name="title" class="form-control" id="inputmailh" value="<?php echo $data[0]->{"title"};?>"> </div>
                        </div>
                        <div class="form-group row"> <label for="inputtext" class="col-form-label">Краткая информация</label>
                            <textarea class="form-control" name="pre_text"  rows="5" id="comment"><?php echo $data[0]->{"pre_text"};?></textarea>
                        </div>
                        <div class="form-group row"> <label for="inputtext" class="col-form-label">Полная информация</label>
                            <textarea class="form-control" name="full_text" rows="5" id="comment"><?php echo $data[0]->{"text"};?></textarea>
                        </div>
                        <div class="form-group row"> <label for="inputtext" class="col-2 col-form-label">Категория новости</label>
                            <div class="form-group">
                                <select class="form-control" name="category">
                                    <option value="0">Без категории</option>
                                    <?php
                                        for ($i = 1; $i <= $data[1]->{'max_load'}; $i++)
                                        {
                                            $select="";
                                            if($i==($data[0]->{"category"})){$select="selected";}
                                            echo "<option ".$select." value=\"".$data[1]->{'result'.$i}->{'id'}."\">".$data[1]->{'result'.$i}->{'name'}."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"> <label for="inputtext" class="col-form-label">Дата и время публикации</label>
                            <input type="date" name="date" value="<? echo date("Y-m-d", strtotime($data[0]->{"delay_public_time"}));?>">
                            <input type="time" name="time" value="<? echo date("H:i", strtotime($data[0]->{"delay_public_time"}));?>"><br/>
                            <small class="text-danger">Если время и дата публикации не указанно, публикация пройдет сразу после создания данной новости.</small>
                        </div>
                        <br>
                        <br>
                        <div class="text-center">
                            <button type="submit" name="EditNews" class="btn btn-primary">Изменить</button>
                            <button type="reset" class="btn btn-primary" onclick="location.href='<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/news/";?>'">Вернуться</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>