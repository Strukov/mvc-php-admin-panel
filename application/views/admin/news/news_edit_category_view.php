<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Лента новостей</h3>
        <br><br>
    </div>

    <?php $getURLid=Route::GetNextValueUrl("id");?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <div class="alert alert-primary">
                    <h4 class="alert-heading">Редактирование категории</h4>
                    <form method="post">
                        <div class="form-group">
                            <label>Название категории</label>
                            <input type="text" name="newtext" class="form-control" placeholder="Введите новое название категории" value="<?php echo News::GetNameNewsCategory($getURLid);?>">
                            <small class="form-text <?php if($data["error"]==0){echo "text-muted";}else{echo "text-danger";}?>">
                                <?php
                                    if($data["error"]==0){
                                        echo "Вы сможете в любой момент изменить название или удалить созданную категорию.";
                                    }else{
                                        echo "Вы не можете оставить данное поле пустым!";
                                    }
                                ?>
                            </small>
                        </div>
                        <button type="submit" class="btn btn-primary" name="editCategory" onclick="location.href='<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/news/category/";?>'">Изменить</button>
                        <button type="reset" class="btn btn-primary" onclick="location.href='<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/news/category/delete/".Route::GetNextValueUrl("id");?>'">Удалить</button>
                        <button type="reset" class="btn btn-primary" onclick="location.href='<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/news/category/";?>'">Вернуться</button>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <h3 class="text-center">Список категорий</h3>
                <br>
                <div class="text-center">
                    <?php
                    for ($i = 1; $i <= $data[0]->{'load_rows2'}; $i++)
                        {
                            echo "
                                <a class=\"btn btn-light\" style=\"width:100%\" href=\"http://".$_SERVER["HTTP_HOST"]."/admin/news/editcategory/id/".$data[0]->{'result_cat'.$i}->{'id'}."\">".$data[0]->{'result_cat'.$i}->{'name'}."</a>
                            ";
                        }
                    ?>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <ul class="pagination" style="margin-top:4%; margin-left: 25%;">
                            <?php
                            $list = $data[0]->{'use_list2'};
                            $addurl="category";
                            if(Route::FindEnteredUrl("addcategory")==true){
                                $addurl="addcategory";
                            }
                            if(Route::FindEnteredUrl("editcategory")==true){
                                $addurl="editcategory/id/".$getURLid;
                            }
                            Listing::ViewListing($list,$data[0]->{'max_load_rows2'},"/admin/news/".$addurl."/list_cat/");
                            ?>
                        </ul>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>