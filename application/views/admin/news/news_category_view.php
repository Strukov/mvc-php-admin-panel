<div class="row">
    <div class="col-md-12">
        <h3 class="text-center">Управление категориями</h3>
        <br><br>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8">
                <?php
                    $resut="";
                    if(Route::GetNextValueUrl("addsuccess")>0)$resut="<p class='text-success'>Вы успешно создали новую категорию: ".News::GetNameNewsCategory(Route::GetNextValueUrl("addsuccess")."/p>");
                    if(Route::GetNextValueUrl("editsuccess")>0)$resut="<p class='text-success'>Вы переименовали категорию на: ".News::GetNameNewsCategory(Route::GetNextValueUrl("editsuccess")."</p>");
                    if(Route::GetNextValueUrl("delete")>0)$resut="<p class='text-danger'>Вы удалили категорию: ".News::GetNameNewsCategory(Route::GetNextValueUrl("delete")."</p>");
                    if(strlen($resut)){
                        echo "<div class=\"alert alert-primary\"><h4 class=\"alert-heading\">";
                        echo $resut;
                        echo "</h4></div>";
                    }
                ?>
                <div class="alert alert-primary">
                    <h4 class="alert-heading">Информационная панель</h4>
                    <p class="mb-0">Вы можете создать новую категорию или изменить/удалить существующую категорию.</p>
                    <br>
                    <br>
                    <p class="mb-0">Выберите одну из доступных категорий в правом блоке или создайте новую.</p>
                    <div class="text-right">
                        <a class="btn btn-outline-primary" href="http://<?php echo $_SERVER["HTTP_HOST"];?>/admin/news/addcategory/">Создать категорию</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h3 class="text-center">Список категорий</h3>
                <br>
                <div class="text-center">
                    <?php
                        for ($i = 1; $i <= $data->{'load_rows2'}; $i++)
                        {
                            echo "
                                    <a class=\"btn btn-light\" style=\"width:100%\" href=\"http://".$_SERVER["HTTP_HOST"]."/admin/news/editcategory/id/".$data->{'result_cat'.$i}->{'id'}."\">".$data->{'result_cat'.$i}->{'name'}."</a>
                                ";
                        }
                    ?>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <ul class="pagination" style="margin-top:4%; margin-left: 25%;">
                            <?php
                                $list = $data->{'use_list2'};
                                $addurl="category";
                                if(Route::FindEnteredUrl("addcategory")==true){
                                    $addurl="addcategory";
                                }
                                if(Route::FindEnteredUrl("editcategory")==true){
                                    $addurl="editcategory/id/".Route::GetNextValueUrl("id");
                                }
                                Listing::ViewListing($list,$data->{'max_load_rows2'},"/admin/news/".$addurl."/list_cat/");
                            ?>
                        </ul>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
    </div>
</div>