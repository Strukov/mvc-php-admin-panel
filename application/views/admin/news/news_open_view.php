<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center"><?php echo $data->{"title"};?></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <p class="text-left">Опубликовал: <?php echo DB::GetUserNick($data->{"user"});?></p>
                        </div>
                        <div class="col-md-8 text-right">
                            <?php
                            echo "Дата создания: ".$data->{"create_time"};
                            if($data->{"delay_public_time"}>0&&strtotime($data->{"delay_public_time"})>time()){
                                echo " | <span class=\"text-danger\">Дата публикации: ".$data->{"delay_public_time"}."</span>";
                            }
                            ?>
                        </div>
                    </div>
                    <br/><br/>
                    <p><?php echo nl2br($data->{"text"});?></p>
                    <br/><br/>
                    <details>
                        <summary>Показать миниатюру</summary>
                        <p><?php echo nl2br($data->{"pre_text"});?></p>
                    </details>
                    <br/><br/>
                    <div class="row">
                        <div class="col-md-6">
                            <p class="text-left">Категория: <?php echo News::GetNameNewsCategory($data->{"category"});?></p>
                        </div>
                        <div class="col-md-6">
                            <p class="text-right"><i class="fa fa-1x fa-eye"></i> <?php echo $data->{"views"};?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#myModal">Удалить</button>
                <a class="btn btn-outline-primary" href="<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/news/edit/".$data->{"id"}."";?>">Изменить</a>
                <a class="btn btn-outline-primary" href="<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/news/";?>">На главную</a>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Вы действительно хотите удалить данную новость?</h4>
            </div>
            <br/>
            <div class="text-center">
                <form method="post">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Отменить</button>
                    <button type="submit" name="DelNews" class="btn btn-primary" onclick="location.href='<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/news/";?>'">Удалить</button>
                </form>
            </div>
            <br/>
        </div>
    </div>
</div>

<script>$('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    })</script>