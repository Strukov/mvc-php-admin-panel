<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-8 text-center">
                <h4 style="margin-top:2%"><?php echo Language::GetLang("ACC_CONTROL_ALL_ACC");?></h4>
            </div>
            <div class="col-md-4">
                <button type="button" class="btn" style="margin:3% 31%" onclick="location.href='/admin/accounts/add'"><?php echo Language::GetLang("ACC_CONTROL_CREATE_ACC");?></button>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <form class="form-inline" method="get">
            <div class="form-group" style="margin-right:10px;">
                <input type="text" name="find" class="form-control text-center" placeholder="<?php echo Language::GetLang("ACC_CONTROL_FIND_KEY");?>" >
            </div>
            <button type="submit" class="btn btn-primary "><?php echo Language::GetLang("BUTTON_FIND");?></button>
            <button type="reset" class="btn btn-primary mx-2" onclick="location.href='/admin/accounts/'"><?php echo Language::GetLang("BUTTON_TO_CANCEL");?></button>
        </form>
        <br>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <?php
                if(isset($_REQUEST['create'])){
                    if($_REQUEST['create']==1){
                        echo "
                        <div class=\"alert alert-primary\" role=\"alert\">
                            <button type=\"button\" class=\"close\" data-dismiss=\"alert\">×</button>
                            <h4 class=\"alert-heading\">".Language::GetLang("ACC_CONTROL_ACC_CREATED")."</h4>
                            <p class=\"mb-0\">".Language::GetLang("ACC_CONTROL_GOOD_CREATE_ACC")."</p>
                        </div>
                        ";
                    }
                }
            ?>
            <table class="table table-bordered table-hover">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th><?php echo Language::GetLang("ACC_CONTROL_NICK");?></th>
                    <th>Email</th>
                    <th><?php echo Language::GetLang("ACC_CONTROL_GROUP");?></th>
                    <th><?php echo Language::GetLang("ACC_CONTROL_REG_IP");?></th>
                    <th><?php echo Language::GetLang("ACC_CONTROL_USE_IP");?></th>
                    <th><?php echo Language::GetLang("ACC_CONTROL_REG_DATE");?></th>
                    <th><?php echo Language::GetLang("ACC_CONTROL_LAST_LOGIN");?></th>
                    <th><?php echo Language::GetLang("ACC_CONTROL_STATUS");?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $textresult="";
                    $max_list=1;
                    for ($i = 1; $i <= $data->{'maxdata'}; $i++)
                    {
                        $textresult.="
                            <tr class=\"movelink\" data-href=\"http://".$_SERVER["HTTP_HOST"]."/admin/accounts/open/id/".$data->{'result'.$i}->{'id'}."\">
                            <th>".$data->{'result'.$i}->{'id'}."</th>
                            <td>".$data->{'result'.$i}->{'login'}."</td>
                            <td>".$data->{'result'.$i}->{'email'}."</td>
                            <td>".Language::GetLang("ADMIN_LVL_".DB::GetUserAdminLevel($data->{'result'.$i}->{'id'}))."</td>
                            <td>".$data->{'result'.$i}->{'reg_ip'}."</td>
                            <td>".$data->{'result'.$i}->{'use_ip'}."</td>
                            <td>".$data->{'result'.$i}->{'created_at'}."</td>
                            <td>".$data->{'result'.$i}->{'last_login_at'}."</td>
                        ";
                        if($data->{'result'.$i}->{'active'}==1)$textresult.="<td>".Language::GetLang("STATUS_ACTIVE")."</td>";
                        else if($data->{'result'.$i}->{'archive'}==1)$textresult.="<td>".Language::GetLang("STATUS_ARCHIVE")."</td>";
                        else if($data->{'result'.$i}->{'deleted'}==1)$textresult.="<td>".Language::GetLang("STATUS_DELETED")."</td>";
                        else $textresult.="<td>".Language::GetLang("STATUS_NO_ACTIVE")."</td>";
                        $textresult.="</tr>";
                    }
                    echo $textresult;
                ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <ul class="pagination" style="margin-top:4%; margin-left: 25%;">

            <?php
                $list = $data->{'use_list'};
                $add_find_url="";
                if (isset($_REQUEST['find'])) {
                    $add_find_url="/?find=".$_REQUEST['find'];
                }
                Listing::ViewListing($list,$data->{'max_load_user'},"/admin/accounts/index/list/","",$add_find_url);
            ?>

        </ul>
    </div>
    <div class="col-md-4"></div>
</div>
<script type="text/javascript">
    jQuery( function($) {
        //$('tr:even').css({'background-color' : '#d9d8df'});
        $('tbody tr[data-href]').addClass('clickable').click( function() {
            window.location = $(this).attr('data-href');
        }).find('a').hover( function() {
            $(this).parents('tr').unbind('click');
        }, function() {
            $(this).parents('tr').click( function() {
                window.location = $(this).attr('data-href');
            });
        });
    });
</script>
<style type="text/css">
    .movelink { cursor: pointer; }
</style>