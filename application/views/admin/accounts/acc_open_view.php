<div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-md-3">
                                <img class="img-fluid rounded-circle" alt="img" src="/images/user/<?php echo DB::GetUserAvatar(0,false,$data["sex"],$data["images"]); ?>">
                            </div>
                            <div class="col-md-9">
                                <div class="text-center">
                                    <h3><?php echo $data["login"]; ?></h3>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td><?php echo Language::GetLang("ACC_CONTROL_1");?></td>
                                            <td>
                                                <?php
                                                    $status="";
                                                    if ($data["active"]==1){$status=Language::GetLang("STATUS_ACTIVE");}
                                                    elseif ($data["archive"]==1){$status=Language::GetLang("STATUS_ARCHIVE");}
                                                    elseif ($data["deleted"]==1){$status=Language::GetLang("STATUS_DELETED");}
                                                    else {$status=Language::GetLang("STATUS_NO_ACTIVE");}
                                                    echo $status;
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?php echo Language::GetLang("ACC_CONTROL_2");?></td>
                                            <td><?php echo Language::GetLang("ADMIN_LVL_".DB::GetUserAdminLevel($data["id"])); ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo Language::GetLang("ACC_CONTROL_3");?></td>
                                            <td><?php echo $data["modified_at"]; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo Language::GetLang("ACC_CONTROL_4");?></td>
                                            <td><?php echo DB::GetUserNick($data["modified_by"]); ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-right">
                                    <a class="btn btn-outline-primary" href="<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/accounts/edit/id/".$data['id']."/";?>"><?php echo Language::GetLang("ACC_CONTROL_5");?></a>
                                </div>
                            </div>
                        </div>
                        <br><br><br>
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header text-center"><?php echo Language::GetLang("ACC_CONTROL_6");?></div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <br>
                                            <tbody>
                                            <tr>
                                                <td><?php echo Language::GetLang("ACC_CONTROL_7");?></td>
                                                <td><?php echo $data["created_at"]; ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo Language::GetLang("ACC_CONTROL_8");?></td>
                                                <td><?php echo $data["last_login_at"]; ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo Language::GetLang("ACC_CONTROL_9");?></td>
                                                <td><?php echo $data["reg_ip"]; ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo Language::GetLang("ACC_CONTROL_10");?></td>
                                                <td><?php echo $data["use_ip"]; ?></td>
                                            </tr>
                                            <tr>
                                                <td><?php echo Language::GetLang("ACC_CONTROL_11");?></td>
                                                <td>
                                                    <?php
                                                        if($data["sex"]==1){echo Language::GetLang("ACC_CONTROL_12");}
                                                        else {echo Language::GetLang("ACC_CONTROL_13");}
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>E-mail</td>
                                                <td><?php echo $data["email"]; ?></td>
                                            </tr>
                                            <tr><td></td><td></td></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
            <div class="col-md-12 mb-5 py-5"></div>
        </div>
    </div>
</div>