<div class="py-5" style="">
    <div class="container">
        <div class="row">
            <div class="col-md-12  mb-5 py-5"></div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div class="card">
                            <div class="card-header text-center h2">Создание нового аккаунта</div>
                            <div class="card-body">
                                <form method="post">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <p class="my-2">Ник пользователя</p>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-0"><input type="name" name="nick" class="form-control"> </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="my-2">E-mail пользователя</p>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-0"><input type="email" name="email" class="form-control"> </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="my-2">Пароль пользователя</p>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-0"><input type="password" name="pass" class="form-control"> </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="my-2">Пол пользователя</p>
                                                </td>
                                                <td>
                                                    <div class="form-group my-1">
                                                        <select class="form-control" name="sex">
                                                            <option value="0" selected="">Мужской</option>
                                                            <option value="1">Женский</option>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <br><br>
                                        <div class="text-center">
                                            <label class="btn btn-outline-primary mb-0"><?php echo Language::GetLang("BUTTON_4");?><input type="submit" name="AddAcc" style="display: none;"></label>
                                            <a class="btn btn-outline-primary" href="<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/accounts/index/";?>"><?php echo Language::GetLang("BUTTON_3");?></a>
                                        </div>
                                        <br>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>