<div class="py-5" style="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <form method="POST" action="" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <br><br>
                                    <img id="img-preview" class="img-fluid rounded-circle" width="300" height="300" alt="img" src="/images/user/<?php echo DB::GetUserAvatar(0,false,$data["sex"],$data["images"]); ?>">
                                    <br><br>
                                    <label class="btn btn-file btn-secondary">
                                        <?php echo Language::GetLang("ACC_CONTROL_14");?><input type="file" id="img" style="display: none;" accept="image/*" name="pictures">
                                    </label>
                                </div>
                                <div class="col-md-9">
                                    <div class="text-center">
                                        <h3><?php echo Language::GetLang("ACC_CONTROL_15");?></h3>
                                    </div>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <p style="padding-top:4%"><?php echo Language::GetLang("ACC_CONTROL_16");?></p>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-0">
                                                        <input type="text" name="nick" class="form-control <?php if($data["error"]==1){echo "is-invalid";}?>" value="<?php echo $data["login"]; ?>"> </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p style="padding-top:4%">E-mail</p>
                                                </td>
                                                <td>
                                                    <div class="form-group mb-0">
                                                        <input type="email" name="email" class="form-control <?php if($data["error"]==2){echo "is-invalid";}?>" value="<?php echo $data["email"]; ?>"> </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="my-2"><?php echo Language::GetLang("ACC_CONTROL_1");?></p>
                                                </td>
                                                <td>
                                                    <?php
                                                        $status[4]="";
                                                        if ($data["active"]==1){$status[0]="selected";}
                                                        elseif ($data["archive"]==1){$status[1]="selected";}
                                                        elseif ($data["deleted"]==1){$status[2]="selected";}
                                                        else {$status[3]="selected";}
                                                    ?>
                                                    <div class="form-group my-1">
                                                        <select class="form-control" name="status">
                                                            <option value="1" <?php echo $status[0];?> ><?php echo Language::GetLang("STATUS_ACTIVE");?></option>
                                                            <option value="2" <?php echo $status[1];?> ><?php echo Language::GetLang("STATUS_ARCHIVE");?></option>
                                                            <option value="3" <?php echo $status[2];?> ><?php echo Language::GetLang("STATUS_DELETED");?></option>
                                                            <option value="4" <?php echo $status[3];?> ><?php echo Language::GetLang("STATUS_NO_ACTIVE");?></option>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p class="my-2"><?php echo Language::GetLang("ACC_CONTROL_2");?></p>
                                                </td>
                                                <td>
                                                    <div class="form-group my-2">
                                                        <?php
                                                            $obj=json_decode($_COOKIE['Author']);
                                                            $my_admin_lvl=DB::GetUserAdminLevel($obj->{'id'});
                                                            if($my_admin_lvl<$data["admin"]){
                                                                echo Language::GetLang("ADMIN_LVL_".$data["admin"]);
                                                            }
                                                            else {
                                                                echo "<select class=\"form - control\" name=\"admin\">";
                                                                for ($i = 0; $i < 8; $i++) {
                                                                    if($my_admin_lvl<$i){break;}
                                                                    if ($data["admin"] == $i) {
                                                                        $sel_text = "selected";
                                                                    } else {
                                                                        $sel_text = "";
                                                                    }
                                                                    echo "<option value=\"".$i."\" " . $sel_text . ">" . Language::GetLang("ADMIN_LVL_" . $i) . "</option>";
                                                                }
                                                                echo "</select>";
                                                            }
                                                        ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <p class="my-2"><?php echo Language::GetLang("ACC_CONTROL_11");?></p>
                                        </td>
                                        <td>
                                            <div class="form-group my-1">
                                                <select class="form-control" name="sex">
                                                    <option value="0" <?php if($data["sex"]!=1){echo "selected";} ?>><?php echo Language::GetLang("ACC_CONTROL_13");?></option>
                                                    <option value="1" <?php if($data["sex"]==1){echo "selected";} ?>><?php echo Language::GetLang("ACC_CONTROL_12");?></option>
                                                </select>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="padding-top:3%"><?php echo Language::GetLang("ACC_CONTROL_NEW_PASS");?></p>
                                        </td>
                                        <td>
                                            <div class="form-group my-1">
                                                <input type="password" class="form-control <?php if($data["error"]==3){echo "is-invalid";}?>" name="pass"> </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p style="padding-top:3%"><?php echo Language::GetLang("ACC_CONTROL_RE_NEW_PASS");?></p>
                                        </td>
                                        <td>
                                            <div class="form-group my-1">
                                                <input type="password" class="form-control <?php if($data["error"]==3){echo "is-invalid";}?>" name="repass"> </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="text-center">
                                <label class="btn btn-outline-primary mb-0">
                                    <?php echo Language::GetLang("BUTTON_2");?>
                                    <input type="submit" name="SaveAcc" style="display: none;">
                                </label>
                                <a class="btn btn-outline-primary" href="<?php echo "http://".$_SERVER["HTTP_HOST"]."/admin/accounts/open/id/".$data['id']."/";?>"><?php echo Language::GetLang("BUTTON_3");?></a>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#img').change(function () {
        var input = $(this)[0];
        if (input.files && input.files[0]) {
            if (input.files[0].type.match('image.*')) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#img-preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            } else {
                console.log('error, no img');
            }
        } else {
            console.log('error - img');
        }
    });

    $('#reset-img-preview').click(function() {
        $('#img').val('');
        $('#img-preview').attr('src', '<?php echo DB::GetUserAvatar(0,false,$data["sex"],$data["images"]); ?>');
    });

    $('#form').bind('reset', function () {
        $('#img-preview').attr('src', '<?php echo DB::GetUserAvatar(0,false,$data["sex"],$data["images"]); ?>');
    });
</script>