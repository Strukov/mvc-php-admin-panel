<div class="row">
    <div class="col-md-12">
        <h3 class="text-center"><?php echo Language::GetLang("LOGS_ACTION_LOG");?></h3>
        <br>
        <form class="form-inline" method="get">
            <div class="form-group" style="margin-right:10px;">
                <input type="text" name="find" class="form-control text-center" placeholder="<?php echo Language::GetLang("ACC_CONTROL_FIND_KEY");?>" >
            </div>
            <button type="submit" class="btn btn-primary "><?php echo Language::GetLang("BUTTON_FIND");?></button>
            <button type="reset" class="btn btn-primary mx-2" onclick="location.href='/admin/logs/'"><?php echo Language::GetLang("BUTTON_TO_CANCEL");?></button>
        </form>
        <br>

        <div class="table-responsive">
            <table class="table table-bordered">
                <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Nick</th>
                    <th>IP</th>
                    <th>Date</th>
                    <th class="text-center"><?php echo Language::GetLang("LOGS_ACTION");?></th>
                </tr>
                </thead>
                <tbody>

                <?php
                    $textresult="";
                    $max_list=1;
                    for ($i = 1; $i <= $data->{'maxdata'}; $i++)
                    {
                        $textresult.="<tr>
                            <td>".$data->{'result'.$i}->{'id'}."</td>
                            <td>".DB::GetUserNick($data->{'result'.$i}->{'user'})."</td>
                            <td>".$data->{'result'.$i}->{'ip'}."</td>
                            <td>".$data->{'result'.$i}->{'create_time'}."</td>
                            <td>".Language::GetLang($data->{'result'.$i}->{'action'})." ".$data->{'result'.$i}->{'text'}."</td>
                        ";
                        $textresult.="</tr>";
                    }
                    echo $textresult;
                ?>

                </tbody>
            </table>
            <?php
                $list = $data->{'use_list'};
//                    echo "Всего юзеров: ".$data->{'max_load_user'}."<br>";
//                    echo "Юзеров на экран: ".MAX_DISPLAY_LIST."<br>";
//                    echo "Всего страниц: ".(ceil($data->{'max_load_user'}/MAX_DISPLAY_LIST))."<br>";
//                    echo "Моя страница: ".$list."<br>";
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row pi-draggable" draggable="true">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <ul class="pagination" style="margin-top:4%; margin-left: 25%;">
                    <?php
                        $list = $data->{'use_list'};
                        $add_find_url="";
                        if (isset($_REQUEST['find'])) {
                            $add_find_url="/?find=".$_REQUEST['find'];
                        }
                        Listing::ViewListing($list,$data->{'max_load_user'},"/admin/logs/index/list/","",$add_find_url);
                    ?>
                </ul>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
</div>