<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="content-type" content="text/html" />
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, user-scalable=0" />
    <link rel="shortcut icon" href="/images/logo.png" type="image/x-icon" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/admin/style.css">
    <title>Admin</title>
    <script type="text/javascript" src="/js/jquery.1-7-1.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
</head>

<body>

<header>
</header>

    <div class="container_menu">
        <div class="member_avatar text-center"><p><?php $obj=json_decode($_COOKIE['Author']); echo DB::GetUserNick($obj->{'id'});?></p></div>

        <div class=" member_avatar text-center">
            <img src="/images/user/<?php $obj=json_decode($_COOKIE['Author']); echo DB::GetUserAvatar($obj->{'id'}); ?>" class="rounded-circle" alt="...">
        </div>

        <div class="main_buttons">
            <button type="button" class="btn btn-outline-primary" onclick="location.href='/admin/setting/'"><?php echo Language::GetLang("MENU_2");?></button>
            <button type="button" class="btn btn-outline-primary" onclick="location.href='/admin/exit/'"><?php echo Language::GetLang("MENU_3");?></button>
        </div>

        <div class="main_menu">
            <ul>
                <li><button type="button" class="btn btn-light" onclick="location.href='/admin/accounts/'"><?php echo Language::GetLang("BUTTON_1");?></button></li>
                <li><button type="button" class="btn btn-light" onclick="location.href='/admin/news/'"><?php echo Language::GetLang("BUTTON_NEWS");?></button></li>
                <br/><br/><br/><br/>
                <li><button type="button" class="btn btn-light" onclick="location.href='/admin/logs/'"><?php echo Language::GetLang("BUTTON_LOGS");?></button></li>
            </ul>
        </div>
    </div>

    <div class="container_content">
        <div class="container"  style="	max-width: 100%">
            <?php include 'application/views/'.$content_view; ?>
        </div>
    </div>

<footer>
</footer>

</body>

</html>