<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>One of the strengths project</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link type='text/css' rel='stylesheet' href='/css/style.css' />
    <script type="text/javascript" src="/js/jquery.1-7-1.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>

</head>
<body>
<header>
    <nav class="navbar navbar-default menu">
        <div class="container">

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo "http://".$_SERVER['HTTP_HOST']; ?>" style="color: #f76d50;">One of the strengths project</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li ><a href="<?php echo "http://".$_SERVER['HTTP_HOST']; ?>">главная</a></li>
                    <li ><a href="https://vk.com/ootsp">мы вконтакте</a></li>
                </ul>
            </div>

        </div>
    </nav>
</header>

<div class="content">
    <div class="splash">
        <div class="container">
            <?php include 'application/views/'.$content_view; ?>
        </div>
    </div>

</div>

<footer>
    <hr>
    <div class="container">
        <p class="pull-right" style="text-align: right;">© 2019 OOTSP</p>
    </div>
</footer>

</body>
</html>
