<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>One of the strengths project</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/theme.css" type="text/css">
</head>

<body>

<?php include 'application/views/'.$content_view; ?>

</body>
</html>
