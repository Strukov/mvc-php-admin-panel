<div class="container">
    <div class="row">
        <div class="col-md-12  mb-5 py-5"></div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header text-center"> Авторизация </div>
                        <div class="card-body" >
                                <small class="form-text text-center text-danger">
                                    <?php
                                        if($data['error']==1){
                                            echo "<h6>Не введен E-mail</h6>";
                                        }
                                        else if($data['error']==2){
                                            echo "<h6>Не введен пароль</h6>";
                                        }
                                    ?>
                                </small>
                            <form id="c_form-h" method="POST">
                                <label for="inputmailh" class="col-form-label">E-mail:</label>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Введите E-mail" id="form9">
                                </div>
                                <label for="inputmailh" class="col-form-label">Пароль:</label>
                                <div class="form-group mb-3">
                                    <input type="password" class="form-control" name="pass" placeholder="Введите пароль" id="form10">
                                    <small class="form-text text-muted text-right">
                                        <a href="#"> Восстановить пароль</a>
                                    </small>
                                </div>
                                <button type="submit" class="btn btn-primary" name="LoginUser">Войти</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
        <div class="col-md-12 mb-5 py-5"></div>
        <div class="col-md-12">
            <div class="py-3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="mb-0">© 2017-2019 One of the strengths project. Все права защищены</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>