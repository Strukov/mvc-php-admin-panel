<div class="container">
    <div class="row">
        <div class="col-md-12  mb-5 py-5"></div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header text-center"> Ошибка входа в админ панель! </div>
                        <div class="card-body">
                            <small class="form-text text-center text-danger">
                                <h6>Данный аккаунт не имет прав администратора!</h6>
                            </small>
                            <br><br>
                            <p>Вы вошли под аккаунт: <?php echo $data['name']; ?></p>
                            <br><br>
                            <small class="form-text text-muted text-right">
                                <a href="/exit/">Сменить аккаунт</a>
                            </small>
                            <small class="form-text text-muted text-right">
                                <a href="<?php echo "http://".$_SERVER['HTTP_HOST'];?>">Перейти на главную страницу</a>
                            </small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
        <div class="col-md-12 mb-5 py-5"></div>
        <div class="col-md-12">
            <div class="py-3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="mb-0">© 2017-2019 One of the strengths project. Все права защищены</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>