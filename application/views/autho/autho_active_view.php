<div class="container">
    <div class="row">
        <div class="col-md-12  mb-5 py-5"></div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header text-center">Подтверждение регистрации</div>
                        <div class="card-body">
                            <p>Введите код подтверждения регистрации, который пришел на указанную Вами почту '<?php echo $data["email"]; ?>'</p>
                            <br>
                            <form method="post">
                                <div class="form-group">
                                    <label>Код подтверждения
                                        <?php
                                            if($data["error"]==1){
                                                echo "<span class=\"text-danger\">( Вы ввели неверный код подтверждения! )</span>";
                                            }
                                        ?>
                                    </label>
                                    <input type="text" name="key" class="form-control">
                                </div>
                                <div class="text-center">
                                    <label class="btn btn-outline-primary mb-0">Ввести<input type="submit" name="ActiveUser" style="display: none;"></label>
                                </div>
                            </form>
                            <br>
                            <small class="form-text text-muted text-right">
                                <a href="/exit/">Сменить аккаунт</a>
                            </small>
                            <small class="form-text text-muted text-right">
                                <a href="<?php echo "http://".$_SERVER['HTTP_HOST'];?>">Перейти на главную страницу</a>
                            </small>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
        <div class="col-md-12 mb-5 py-5" style=""></div>
        <div class="col-md-12">
            <div class="py-3">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="mb-0">© 2017-2019 One of the strengths project. Все права защищены</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>