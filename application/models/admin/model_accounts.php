<?php

class Model_Accounts extends Model
{
    public function get_data($num=0)
    {
        if($num==0){
            return self::ResponseIndex();
        }
        else if($num==1){
            return self::ResponseOpen();
        }
        else if($num==2){
            return self::ResponseEdit();
        }
        else if($num==3){
            return self::ResponseAdd();
        }
        return false;
    }

    // открываем стартувую страницу аккаунтов
    static function ResponseIndex()
    {
        $filter="";
        if (isset($_REQUEST['find'])) {
            $filter=$_REQUEST['find'];
        }
        $use_list=Route::GetNextValueUrl("list");
        if($use_list<=0)$use_list=1;
        if(strlen($filter)){
            $obj = json_decode(self::GetAccountModel($use_list,$filter));
        }else{
            $obj = json_decode(self::GetAccountModel($use_list));
        }
        return $obj;
    }

    // открываем просмотр выбранного аккаунта
    static function ResponseOpen()
    {
        $id=Route::GetNextValueUrl("id");
        $query="SELECT * FROM `user` WHERE `id`='".$id."'";
        $result = DB::ConnectBD($query);
        if($result){
            return mysqli_fetch_assoc($result);
        }
        return false;
    }

    // создаем новый аккаунт
    static function ResponseAdd()
    {
        if(isset($_REQUEST['AddAcc'])){
            if(!strlen($_REQUEST['nick'])) {
                $error=array("error"=>"1");
            }
            if(!strlen($_REQUEST['email'])) {
                $error=array("error"=>"2");
            }
            else{
                $result = DB::ConnectBD("SELECT `id` FROM `user` WHERE `email`='".$_REQUEST['email']."'");
                $obj=mysqli_fetch_assoc($result);
                if($obj["id"]>0){
                    $error=array("error"=>"3");
                }
            }
            if(!strlen($_REQUEST['pass'])){
                $error=array("error"=>"4");
            }
            if($error{"error"}==0){
                $token=Route::SetTokenKey();
                $result = DB::ConnectBD("SELECT `id` FROM `user` WHERE `token`='".$token."'");
                $obj=mysqli_fetch_assoc($result);
                while ($obj{"id"}!=0){
                    $token=Route::SetTokenKey();
                    $result = DB::ConnectBD("SELECT `id` FROM `user` WHERE `token`='".$token."'");
                    $obj=mysqli_fetch_assoc($result);
                }
                $query="INSERT INTO `user` (`login`, `password`, `email`, `sex`, `token`, `reg_ip`, `created_at`) 
                        VALUE ('".$_REQUEST['nick']."','".$_REQUEST['pass']."','".$_REQUEST['email']."','".$_REQUEST['sex']."','".$token."','".$_SERVER["REMOTE_ADDR"]."','".date("Y-m-d H:i:s")."')
                ";
                DB::ConnectBD($query);
                Logs::AddLogs("LOGS_CREATE_ACC","Nick: ".$_REQUEST['nick']." | Email: ".$_REQUEST['email']."");
                header('location:http://'.$_SERVER["HTTP_HOST"].'/admin/accounts/index/?create=1');
                return true;
            }
        }
        return $error;
    }

    // открываем редактирование выбранного аккаунта
    static function ResponseEdit()
    {
        $id=Route::GetNextValueUrl("id");
        $query="SELECT * FROM `user` WHERE `id`='".$id."'";
        $result = DB::ConnectBD($query);
        $obj=mysqli_fetch_assoc($result);
        $admin_lvl=DB::GetUserAdminLevel($id);
        $edit_user_id=json_decode($_COOKIE['Author']);
        $admin=array("admin"=>$admin_lvl);
        $error=array();

        if (isset($_REQUEST['SaveAcc'])) {

            $tmp_name = $_FILES["pictures"]["tmp_name"];
            $name = basename($_FILES["pictures"]["name"]);
            if(move_uploaded_file($tmp_name,"./images/user/".$name)){
                $img=$name;
            }
            else{
                $img=$obj["images"];
            }

            if(!strlen($_REQUEST['nick'])) {
                $error=array("error"=>"1");
            }

            if(!strlen($_REQUEST['email'])) {
                $error=array("error"=>"2");
            }
            if($_REQUEST['status']==1){$set_status="`active`='1',`archive`='0',`deleted`='0',";}
            elseif($_REQUEST['status']==2){$set_status="`active`='0',`archive`='1',`deleted`='0',";}
            elseif($_REQUEST['status']==3){$set_status="`active`='0',`archive`='0',`deleted`='1',";}
            else{$set_status="`active`='0',`archive`='0',`deleted`='0',";}

            $newpass="";
            if(strlen($_REQUEST['pass'])) {
                if(strlen($_REQUEST['repass'])) {
                    if($_REQUEST['pass'] != $_REQUEST['repass']){
                        $error=array("error"=>"3");
                    }
                    else{
                        $newpass="`password`='".$_REQUEST['pass']."',";
                    }
                }
                else{
                    $error=array("error"=>"3");
                }
            }
            if($error{"error"}==0){
                $query2="UPDATE `user` SET 
                            `login`='".$_REQUEST['nick']."',
                            `email`='".$_REQUEST['email']."',
                            `images`='".$img."',
                            ".$set_status."
                            ".$newpass."
                            `sex`='".$_REQUEST['sex']."',
                            `modified_at`='".date("Y-m-d H:i:s")."',
                            `modified_by`='".$edit_user_id->{'id'}."'
                            WHERE `id`='".$id."'";
                DB::ConnectBD($query2);
                Logs::AddLogs("LOGS_EDIT_ACC","ID: ".$id." | Nick: ".$_REQUEST['nick']." | Data: {nick: ".$_REQUEST['nick']." | email: ".$_REQUEST['email']." | images: ".$img." | sex: ".$_REQUEST['sex']."}");
                if($admin_lvl!=$_REQUEST['admin'])
                {
                    $result=DB::ConnectBD("SELECT `id` FROM `admin` WHERE `user`='".$id."' AND `active`='1'");
                    if(mysqli_num_rows($result)>0){
                        DB::ConnectBD("UPDATE `admin` SET 
                            `admin_access`='".$_REQUEST['admin']."',
                            `modified_at`='".date("Y-m-d H:i:s")."',
                            `modified_by`='".$edit_user_id->{'id'}."' 
                            WHERE `user`='".$id."' AND `active`='1'");
                        Logs::AddLogs("LOGS_SET_ADMIN","Nick: ".$_REQUEST['nick']." | LVL: ".$_REQUEST['admin']."");
                    }
                    else {
                        DB::ConnectBD("INSERT INTO `admin` (`user`, `admin_access`, `active`, `archive`, `deleted`,`created_at`,`modified_by`) 
                        VALUES ('".$id."','".$_REQUEST['admin']."','1','0','0','".date("Y-m-d H:i:s")."','".$edit_user_id->{'id'}."')");
                        Logs::AddLogs("LOGS_GIVE_ADMIN","Nick: ".$_REQUEST['nick']." | LVL: ".$_REQUEST['admin']."");
                    }
                }
                header('location:http://'.$_SERVER["HTTP_HOST"].'/admin/accounts/open/id/'.$id);
                return true;
            }
        }
        if($result){
            return $obj+$admin+$error;
        }
        return false;
    }

    // Вывод всех аккаунтов в список
    static function GetAccountModel($use_list=1,$filter="")
    {
        $usefilter="";
        if(strlen($filter))$usefilter="WHERE `login` LIKE '%".$filter."%' OR `email` LIKE '%".$filter."%'";
        $result = DB::ConnectBD("SELECT * FROM `user` ".$usefilter."LIMIT ".(($use_list-1)*MAX_DISPLAY_LIST).",".(MAX_DISPLAY_LIST+1));
        if(!strlen($filter)){
            $result2 = DB::ConnectBD("SELECT `id` FROM `user`");
        }
        else{
            $result2 = DB::ConnectBD("SELECT `id` FROM `user`".$usefilter);
        }
        $data=1;
        $ss=array();
        if($result) {
            $rows = mysqli_num_rows($result);
            if($rows>MAX_DISPLAY_LIST)$max=$rows-1;
            else $max=$rows;
            for ($i = 1; $i <= $max; $i++) {
                $json=array("result".$i=>(mysqli_fetch_assoc($result)));
                $ss = $ss+($json);
                $data+=1;
            }
            $ss= $ss+(array("maxuser"=>$rows,"maxdata"=>--$data,"max_load_user"=>mysqli_num_rows($result2),"use_list"=>$use_list));
        }
        return json_encode($ss);
    }
}