<?php

class Model_Logs extends Model
{
    public function get_data()
    {
        $filter="";
        if (isset($_REQUEST['find'])) {
            $filter=$_REQUEST['find'];
        }
        $use_list=Route::GetNextValueUrl("list");
        if($use_list<=0)$use_list=1;
        if(strlen($filter)){
            $obj = json_decode(self::GetLogsModel($use_list,$filter));
        }else{
            $obj = json_decode(self::GetLogsModel($use_list));
        }
        return $obj;
    }

    // Вывод всех аккаунтов в список
    static function GetLogsModel($use_list=1,$filter="")
    {
        $usefilter="";
        if(strlen($filter))$usefilter="WHERE `text` LIKE '%".$filter."%'";
        $result = DB::ConnectBD("SELECT * FROM `logs` ".$usefilter."ORDER BY `id` DESC LIMIT ".(($use_list-1)*MAX_DISPLAY_LIST).",".(MAX_DISPLAY_LIST+1));
        if(!strlen($filter)){
            $result2 = DB::ConnectBD("SELECT `id` FROM `logs`");
        }
        else{
            $result2 = DB::ConnectBD("SELECT `id` FROM `logs`".$usefilter);
        }
        $data=1;
        $ss=array();
        if($result) {
            $rows = mysqli_num_rows($result);
            if($rows>MAX_DISPLAY_LIST)$max=$rows-1;
            else $max=$rows;
            for ($i = 1; $i <= $max; $i++) {
                $json=array("result".$i=>(mysqli_fetch_assoc($result)));
                $ss = $ss+($json);
                $data+=1;
            }
            $ss= $ss+(array("maxuser"=>$rows,"maxdata"=>--$data,"max_load_user"=>mysqli_num_rows($result2),"use_list"=>$use_list));
        }
        return json_encode($ss);
    }
}