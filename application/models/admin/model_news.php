<?php

class Model_News extends Model
{
    // таблица в базе данных
    static function GetTable()
    {
        return "news";
    }
    static function GetTableCategory()
    {
        return "news_category";
    }

    public function get_data($num)
    {
        if($num==1){
            return self::ResponseAdd();
        }
        elseif($num==2){
            return self::ResponseOpen();
        }
        elseif($num==3){
            return self::ResponseEdit();
        }
        elseif($num==4){
            return self::ResponseCategory();
        }
        elseif($num==5){
            return self::ResponseAddCategory();
        }
        elseif($num==6){
            return self::ResponseEditCategory();
        }
        else{
            return self::ResponseIndex();
        }
    }








    // открываем стартувую страницу ленты новостей
    static function ResponseIndex()
    {
        $filter="";
        if (isset($_REQUEST['find'])) {
            $filter=$_REQUEST['find'];
        }
        $use_list=Route::GetNextValueUrl("list");
        if($use_list<=0)$use_list=1;
        $use_list2=Route::GetNextValueUrl("list_cat");
        if($use_list2<=0)$use_list2=1;
        if(strlen($filter)){
            $obj = self::GetNewsInfo($use_list,$filter);
        }else{
            $obj = self::GetNewsInfo($use_list);
        }
        $success=Route::GetNextValueUrl("success");
        $obj=array(json_decode($obj),json_decode(self::GetNewsCategoryInfo($use_list2)),"success"=>$success);
        return $obj;
    }

    // Создание новой новости
    static function ResponseAdd()
    {
        $obj=News::GetAllNameNewsCategory();
        $error=array();
        if (isset($_REQUEST['AddNews'])) {
            if (!strlen($_REQUEST['title'])) {
                $error=array("error"=>"1");
            }
            if (!strlen($_REQUEST['pre_text'])) {
                $error=array("error"=>"2");
            }
            if (!strlen($_REQUEST['full_text'])) {
                $error=array("error"=>"3");
            }
            $cookie=json_decode($_COOKIE['Author']);
            if($error["error"]==0){
                DB::ConnectBD("INSERT INTO `news` (`user`,`title`, `pre_text`, `text`, `category`, `create_time`, `delay_public_time`, `active`) 
                                        VALUES ('".$cookie->{'id'}."',
                                                '".$_REQUEST['title']."',
                                                '".$_REQUEST['pre_text']."',
                                                '".$_REQUEST['full_text']."',
                                                '".$_REQUEST['category']."',
                                                '".date("Y-m-d H:i:s")."',
                                                '".date($_REQUEST['date']." ".$_REQUEST['time'])."',
                                                '1')",$con);
                header('location:http://'.$_SERVER["HTTP_HOST"].'/admin/news/index/success/'.$con);
                return true;
            }
        }
        $obj = array(json_decode($obj),$error);
        return $obj;
    }


    // Открыть новость
    static function ResponseOpen()
    {
        $id=Route::GetNextValueUrl("open");
        if (isset($_REQUEST['DelNews'])) {
            DB::ConnectBD("UPDATE `news` SET `active`='0',`archive`='0',`deleted`='1' WHERE `id`='".$id."'");
            header('location:http://'.$_SERVER["HTTP_HOST"].'/admin/news/');
            return true;
        }
        return json_decode(News::OpenNewsId($id));
    }

    // Изменить новость
    static function ResponseEdit()
    {
        $id=Route::GetNextValueUrl("edit");
        $error=array();
        if (isset($_REQUEST['EditNews'])) {
            if (!strlen($_REQUEST['title'])) {
                $error=array("error"=>"1");
            }
            if (!strlen($_REQUEST['pre_text'])) {
                $error=array("error"=>"2");
            }
            if (!strlen($_REQUEST['full_text'])) {
                $error=array("error"=>"3");
            }
            if($error["error"]==0){
                $title=htmlspecialchars($_REQUEST['title']." ", ENT_QUOTES);
                $full=htmlspecialchars($_REQUEST['full_text']." ", ENT_QUOTES);
                $pre_full=htmlspecialchars($_REQUEST['pre_text']." ", ENT_QUOTES);
                $query="UPDATE `news` SET `title`='".$title."',
                                          `text`='".$full."',
                                          `pre_text`='".$pre_full."',
                                          `category`='".$_REQUEST['category']."',
                                          `delay_public_time`='".date($_REQUEST['date']." ".$_REQUEST['time'])."' 
                        WHERE `id`='".$id."'";
                DB::ConnectBD($query);
                header('location:http://'.$_SERVER["HTTP_HOST"].'/admin/news/open/'.$id);
                return true;
            }
        }
        $obj = array(json_decode(News::OpenNewsId($id)),json_decode(News::GetAllNameNewsCategory()),$error["error"]);
        return $obj;
    }

    // Главная страница категорий
    static function ResponseCategory()
    {
        $delete=Route::GetNextValueUrl("delete");
        if($delete>0){
            DB::ConnectBD("UPDATE `".self::GetTableCategory()."` SET `active`='0',`archive`='0',`deleted`='1' WHERE `id`='".$delete."'");
            $result=DB::ConnectBD("SELECT `id` FROM `".self::GetTable()."` WHERE `category`='".$delete."'");
            if($result){
                $rows = mysqli_num_rows($result);
                for ($i = 1; $i <= $rows; $i++) {
                    $editobj=mysqli_fetch_assoc($result);
                    DB::ConnectBD("UPDATE `".self::GetTable()."` SET `category`='0' WHERE `id`='".$editobj["id"]."'");
                }
            }
            Logs::AddLogs("LOGS_DELETE_NEWS_CATEGORY","ID category [".$delete."] | Name: [".News::GetNameNewsCategory($delete)."]");
        }
        $use_list2=Route::GetNextValueUrl("list_cat");
        if($use_list2<=0)$use_list2=1;
        $obj=json_decode(self::GetNewsCategoryInfo($use_list2));
        return $obj;
    }

    // Создание новой категории
    static function ResponseAddCategory()
    {
        if (isset($_REQUEST['addCategory'])) {
            if(!strlen($_REQUEST['addname'])){
                $error=1;
            }else{
                DB::ConnectBD("INSERT INTO `".self::GetTableCategory()."` (`name`,`active`) VALUES ('".$_REQUEST['addname']."','1')",$con);
                Logs::AddLogs("LOGS_ADD_NEWS_CATEGORY", "ID category [" . $con . "] | Name [" . $_REQUEST['addname'] . "]");
                header('location:http://' . $_SERVER["HTTP_HOST"] . '/admin/news/category/addsuccess/' . $con);
                return true;
            }
        }
        $use_list2=Route::GetNextValueUrl("list_cat");
        if($use_list2<=0)$use_list2=1;
        $obj = array(json_decode(self::GetNewsCategoryInfo($use_list2)),"error"=>$error);
        return $obj;
    }

    // Редиктирование категорий
    static function ResponseEditCategory()
    {
        $id=Route::GetNextValueUrl("id");
        if($id<=0){
            header('location:http://'.$_SERVER["HTTP_HOST"].'/admin/news/category/');
            return true;
        }
        if (isset($_REQUEST['editCategory'])) {
            if (!strlen($_REQUEST['newtext'])) {
                $error = 1;
            } else {
                DB::ConnectBD("UPDATE `" . self::GetTableCategory() . "` SET `name`='" . $_REQUEST['newtext'] . "' WHERE `id`='" . $id . "'");
                Logs::AddLogs("LOGS_EDIT_NEWS_CATEGORY", "ID category [" . $id . "] | Name [" . $_REQUEST['newtext'] . "]");
                header('location:http://' . $_SERVER["HTTP_HOST"] . '/admin/news/category/editsuccess/' . $id);
                return true;
            }
        }
        $use_list2=Route::GetNextValueUrl("list_cat");
        if($use_list2<=0)$use_list2=1;
        $obj = array(json_decode(self::GetNewsCategoryInfo($use_list2)),"error"=>$error);
        return $obj;
    }

    // получаем все категории
    static function GetNewsCategoryInfo($use_list=1)
    {
        $result = DB::ConnectBD("SELECT * FROM `".self::GetTableCategory()."` WHERE `active`='1' LIMIT ".(($use_list-1)*MAX_DISPLAY_LIST).",".(MAX_DISPLAY_LIST+1));
        $result2 = DB::ConnectBD("SELECT `id` FROM `".self::GetTableCategory()."`  WHERE `active`='1'");
        $data=1;
        $ss=array();
        if($result) {
            $rows = mysqli_num_rows($result);
            if($rows>MAX_DISPLAY_LIST)$max=$rows-1;
            else $max=$rows;
            for ($i = 1; $i <= $max; $i++) {
                $json=array("result_cat".$i=>(mysqli_fetch_assoc($result)));
                $ss = $ss+($json);
                $data+=1;
            }
            $ss= $ss+(array("load_list2"=>$rows,"load_rows2"=>--$data,"max_load_rows2"=>mysqli_num_rows($result2),"use_list2"=>$use_list));
        }
        return json_encode($ss);
    }

    // Вывод всех аккаунтов в список
    static function GetNewsInfo($use_list=1,$filter=0)
    {
        $usefilter="";
        if($filter>0){
            $usefilter="
            AND `category`='".$filter."'
            ";
        }
        $result = DB::ConnectBD("SELECT * FROM `".self::GetTable()."` WHERE `active`='1' ".$usefilter." LIMIT ".(($use_list-1)*MAX_DISPLAY_LIST).",".(MAX_DISPLAY_LIST+1));
        if($filter<=0){
            $result2 = DB::ConnectBD("SELECT `id` FROM `".self::GetTable()."` WHERE `active`='1'");
        }
        else{
            $result2 = DB::ConnectBD("SELECT `id` FROM `".self::GetTable()."` WHERE `active`='1'".$usefilter);
        }
        $data=1;
        $ss=array();
        if($result) {
            $rows = mysqli_num_rows($result);
            if($rows>MAX_DISPLAY_LIST)$max=$rows-1;
            else $max=$rows;
            for ($i = 1; $i <= $max; $i++) {
                $json=array("result".$i=>(mysqli_fetch_assoc($result)));
                $ss = $ss+($json);
                $data+=1;
            }
            $ss= $ss+(array("load_list"=>$rows,"load_rows"=>--$data,"max_load_rows"=>mysqli_num_rows($result2),"use_list"=>$use_list));
        }
        return json_encode($ss);
    }
}