<?php

class Model_Autho extends Model
{
	
	public function get_data($num)
	{
        if($num==1){
            return self::RequestDeny();
        }
        if($num==2){
            return self::RequestActive();
        }
        else{
            return self::RequestLogin();
        }
	}
    static function RequestLogin()
    {
        // если ты уже авторизирован, то перекидываем на гл. страницу
        if(isset($_COOKIE['Author'])){
            if(DB::GetAuthorizationUser($_COOKIE['Author'])){
                $obj=json_decode($_COOKIE['Author']);
                if(DB::GetUserStatus($obj->{"id"})==0){
                    header('location:http://'.$_SERVER['HTTP_HOST']."/autho/active");
                    return true;
                }
                header('location:http://'.$_SERVER['HTTP_HOST']);
                return true;
            }
        }

        if (isset($_REQUEST['LoginUser'])){
            if (!strlen($_REQUEST['email'])){
                $result=array('error' => '1');
                return $result;
            }
            if (!strlen($_REQUEST['pass'])){
                $result=array('error' => '2');
                return $result;
            }
            $result=array(
                'email' => $_REQUEST['email'],
                'pass' => $_REQUEST['pass']
            );
            DB::AuthorizationUser($result);
        }
        return true;
    }
    static function RequestDeny()
    {
        $obj=json_decode($_COOKIE['Author']);
        $result=array('name' => DB::GetUserNick($obj->{"id"}));
        return $result;
    }
    static function RequestActive()
    {
        $obj=json_decode($_COOKIE['Author']);
        $response=DB::ConnectBD("SELECT `email`,`token` FROM `user` WHERE `id`='".$obj->{"id"}."'");
        if($response){
            $obj2=mysqli_fetch_assoc($response);
            $result=array('email' => $obj2['email'],"token"=>$obj2['token']);
        }
        if(isset($_REQUEST['ActiveUser'])){
            if(!strlen($_REQUEST['key'])){
                $result+=array('error' => '1');
            }
            else{
                if($obj2['token']==$_REQUEST['key']){
                    DB::ConnectBD("UPDATE `user` SET `active`='1', `archive`='0', `deleted`='0' WHERE `id`='".$obj->{"id"}."'");
                    header('location:http://'.$_SERVER['HTTP_HOST']);
                    return true;
                }else{
                    $result+=array('error' => '1');
                }
            }
        }
        return $result;
    }
}
